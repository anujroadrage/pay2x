var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';


 function onClickBuyNow()
{
	var temp= this.tag;
}

function onCheckClicked(e)
{
	var amount=0;
	
	if($("#addServicePage  #service_tier1").is(":checked"))
	{
		amount+=1000;
	}
	
	if($("#addServicePage  #service_tier2").is(":checked"))
	{
		amount+=1000;
	}
	
	document.getElementById("addServicePage").querySelector("#total_amount").innerHTML = "Total Amount : "+amount;
	
}


$(document).ready(function(){

/* ///// Merchant Info ///// */



$("#addServicePage #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#addServicePage";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_tier1 	=	$(elementId+" #service_tier1").is(":checked")?1:0;
	
	
	// if all service tier are zero then show message

    if (_service_tier1 == 0) {
		showAlertOnMessagePost("Please choose atleast one services",2,elementId);
    }
    else {
		var form_data = 
		{
			service_tier1	:	_service_tier1,
		};
		callService(form_data,elementId);
    }
  });
 


 
 /* ///  Service Call // */
 
 function callService(form_data,id="")
 {
	 $.post("php/addServicePage.php", form_data, function(data){

		var jsonData = JSON.parse(data);
		if(jsonData.result == 1)
		{
			showAlertOnMessagePost("Data is posted",1,id);
		}
		else
		{
			showAlertOnMessagePost("ERROR => "+jsonData.message,2,id);
		}
		
      });
 }
 
 										
	
 
/* 	
	SUCCESS:1,
	INCOMPLETE_DATA:2,
	FAILED:3,
	NONE:4 
*/
	


  function showAlertOnMessagePost(message,msg_type,id="")
  {
	if(msg_type == 1)
	{	
		 $(id +" #success").removeClass("success_none");
      if (!$(id +" #success").hasClass("success")) {
        $(id +" #success").addClass("success");
      }
	}
	else if(msg_type == 2)
	{	
		$(id +" #success").removeClass("success_none");
		if (!$(id +" #success").hasClass("alert-display")) {
		  $(id +" #success").addClass("alert-display");
		}
	}
	else if(msg_type == 4)
	{	
		if (!$(id +" #success").hasClass("alert-display")) {
			$(id +" #success").removeClass("alert-display");
		}
		if (!$(id +" #success").hasClass("success")) {
			$(id +" #success").removeClass("success");
		}
		$(id +" #success").addClass("success_none");	
	}
	
    $(id +" #success").html(message);
  }

  
 

});
