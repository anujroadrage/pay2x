var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';

 //checking if user is already login or not
 function loginCheckStatus(){
   $(document).ready(function(){
     $.post("php/logincheck.php", function(data){
       var myobj = JSON.parse(data);

       //checking cookies whether these are set or not
       if (myobj.cookie_name != null && myobj.cookie_password != null) {
         if (myobj.cookie_name != "" && myobj.cookie_password != "") {
           $("#login-username").attr("value", myobj.cookie_name);
           $("#login-password").attr("value", myobj.cookie_password);
         }
       }

       if (myobj.status == 1) {
        $("#login_username a").html("Hi, "+myobj.username);
        $("#header_signup_btn").css("display", "none");
		$("#header_login_btn").css("display", "none");
        $("#login_username").css("display", "");
		$("#login_status").css("display", "");
       }
       else if (myobj.status == 0) {
        $("#header_signup_btn").css("display", "");
		$("#header_login_btn").css("display", "");
        $("#login_username").css("display", "none");
		$("#login_status").css("display", "none");
              
		if (myobj.modal == 1) {
           $("#myModal").modal("show");
        }
       }
     });
   });
 }

 loginCheckStatus(); //calling function

function showAlert(id,noAlertClass, alertClass, message)
{
	$(id).removeClass(noAlertClass);
		if (!($(id).hasClass(alertClass))) {
			$(id).addClass(alertClass);
		}
	$(id).html(message);
}

$(document).ready(function(){

  //loging in user
  $("#btnlogin").click(function(){
    var username = $("#login-username").val();
    var password = $('#login-password').val();
	
	var id="#login-alert";
	var noAlertClass ="alert-disp";
	var alertClass ="alert-disp-1";
	
	
    if (username.trim() == "" && password.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Username or Password cannot be empty!");
    }
    else if (username.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Username cannot be empty!");
    }
    else if (password.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Password cannot be empty!");
    }
    else {
      $.post("php/login.php", $("#loginform :input").serializeArray(), function(response){
		  
		  var data = JSON.parse(response);
		  
        if(data.result == 1){
        $("#login_username a").html("Hi, "+data.username);
        $("#header_signup_btn").css("display", "none");
		$("#header_login_btn").css("display", "none");
        $("#login_username").css("display", "");
		$("#login_status").css("display", "");
		 $("#myModal").modal("hide");
        }
        else if (data.result != 1) {
         showAlert(id,noAlertClass,alertClass,data.message);
        }
      });
    }
  });

  //sign up
  $("#btn-signup").click(function(){
    var email = $("#signup-email").val();
    var username = $("#signup-username").val();
    var firstname = $("#signup-firstname").val();
    var lastname = $("#signup-lastname").val();
    var password = $("#signup-password").val();
    var repassword = $("#signup-repassword").val();
    var captcha = $("#signup-captcha").val();
    password = password.trim();
    password = password.trim();

	var id="#signupalert";
	var noAlertClass ="alert-signup";
	var alertClass ="alert-signup-1";
	
    //for password validation
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

    if (email.trim() == "" &&
        username.trim() == "" &&
        firstname.trim() == "" &&
        lastname.trim() == "" &&
        password.trim() == "" &&
        repassword.trim() == "" &&
        captcha.trim() == "") {
		
		showAlert(id,noAlertClass,alertClass,"All fields are required");		
    }
    else if (email.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Email cannot be empty");
    }
    else if (username.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Username cannot be empty");
	}
    else if (firstname.trim() == "") {
    showAlert(id,noAlertClass,alertClass,"First Name cannot be empty");
	}
    else if (lastname.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Last Name cannot be empty");
    }
    else if (password.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Password cannot be empty");
    }
    else if (repassword.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Retype Password cannot be empty");
    }
    else if (captcha.trim() == "") {
		showAlert(id,noAlertClass,alertClass,"Captcha cannot be empty");
    }
	//password validation
    else if (!password.match(decimal)) {
		  showAlert(id,noAlertClass,alertClass,"Password must be between 8 to 15 characters which" +
        "contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character");
    }
	else {
		$.post("php/signup.php", $("#signupform :input").serializeArray(), function(response){
			
			var data = JSON.parse(response);
			 
		  if (data.result ==  1) {
			$("#login_username a").html("Hi, "+myobj.username);
			$("#header_signup_btn").css("display", "none");
			$("#header_login_btn").css("display", "none");
			$("#login_username").css("display", "");
			$("#login_status").css("display", "");
			 $("#myModal").modal("hide");
		  }
		  else if (data.result != 1) {
			$("#signupalert").removeClass("alert-signup");
			if (!$("#signupalert").hasClass("alert-signup")) {
			  $("#signupalert").addClass("alert-signup-1");
			}
			$("#signupalert").html(data.message);
		  }
		});
	}
    
  });

  $("#basic_info_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#basic_info_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _name 	= 	$(elementId+" #name").val();
	var _email	=  	$(elementId+" #email").val();
	var _phone	=	$(elementId+" #phone").val();
	var _dob	= 	$(elementId+" #dob").val();
	
	if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_dob.trim() == "") {
		showAlertOnMessagePost("Please Enter dob",2,elementId);
    }
    else {
		
		var form_data = 
		{
			name 				: 	_name,
			email 				: 	_email,
			phone 				: 	_phone,
			dob 				: 	_dob	
		};
		
		$.post("php/basic_info.php", form_data, function(data){
        
		showAlertOnMessagePost("Data is posted",1,elementId);
		
		var jsonData = JSON.parse(data);
		
		if(jsonData.result == 1)
		{
			$("#basic_info_modal").modal('hide');
		}
        else if (jsonData.result != 1) {
          
		  $("#success").removeClass("success_none");
          if (!($("#success").hasClass("alert-display"))) {
            $("#success").addClass("alert-display");
          }
          $("#success").html(jsonData.message);
		  
        }
      });
     
    }
  });
  
  
  //contact us
  $("#btnContactUs").click(function(){
    //getting values of the fields
    
	var name = $("#contact-us-name").val();
    var email = $("#contact-us-email").val();
    var subject = $("#contact-us-subject").val();
    var message = $("#contact-us-message").val();
	
    name = name.trim();
    email = email.trim();
    message = message.trim();

    if (name == "" && email == "" && subject == null && message == "") {
      alert("All fields are required");
    }
    else if (name == "") {
      alert("Name cannot be empty");
    }
    else if (email == "") {
      alert("E-mail cannot be empty");
    }
    else if (subject == null) {
      alert("Subject cannot be empty");
    }
    else if (message == "") {
      alert("Message cannot be empty");
    }
    else {
      $.post("php/contact.php",
      {
        name: name,
        e_mail: email,
        subject: subject,
        message: message
      },
      function(data){
        if (data == "We will contact you soon via E-mail") {
          alert(data);
        }
        else {
          alert(data);
        }
      });
    }
  });

 
});


 
 

function showAlertOnMessagePost(message,msg_type,id="")
  {
	if(msg_type == 1)
	{	
		 $(id +" #success").removeClass("success_none");
      if (!$(id +" #success").hasClass("success")) {
        $(id +" #success").addClass("success");
      }
	}
	else if(msg_type == 2)
	{	
		$(id +" #success").removeClass("success_none");
		if (!$(id +" #success").hasClass("alert-display")) {
		  $(id +" #success").addClass("alert-display");
		}
	}
	else if(msg_type == 4)
	{	
		if (!$(id +" #success").hasClass("alert-display")) {
			$(id +" #success").removeClass("alert-display");
		}
		if (!$(id +" #success").hasClass("success")) {
			$(id +" #success").removeClass("success");
		}
		$(id +" #success").addClass("success_none");	
	}
	
    $(id +" #success").html(message);
  }
  
  

