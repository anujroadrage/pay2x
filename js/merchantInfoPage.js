var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';


 function onClickBuyNow()
{
	var temp= this.tag;
}


$(document).ready(function(){


/* ///// Merchant Info ///// */

$("#merchantInfoPage #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#merchantInfoPage";
	showAlertOnMessagePost("",4,elementId);
	
	var _first_name 	=	$(elementId+" #first_name").val();
	var _last_name 		=	$(elementId+" #last_name").val();
	var _dob			= 	verifyDOB(elementId);
	var _first_phone	=	$(elementId+" #first_phone").val();
	var _sec_phone		=	$(elementId+" #sec_phone").val();
	var _email			=  	verifyEmail(elementId);
	
	var _aadhar_num		=	$(elementId+" #aadhar_num").val();
	var _gender			=	$(elementId+" #gender").val();
	var _maritial_status=	$(elementId+" #maritial_status").val();
	var _pancard_num	=	$(elementId+" #pancard_num").val();
	var _address		=	$(elementId+" #address").val();
	var _currentcity	=	$(elementId+" #currentcity").val();
	var _pincode		= 	verifyPincode(elementId);
	
	var _bank 			=	$(elementId+" #prim_bank").val();
	var _account_num 	=	$(elementId+" #account_num").val();
	var _account_name 	=	$(elementId+" #account_name").val();
	var _ifsc_code 		=	$(elementId+" #ifsc_code").val();
	
	var _aadhar_file 	=	$(elementId+" #aadhar_file").val();
	var _pancard_file 	=	$(elementId+" #pancard_file").val();
	var _signed_form 	=	$(elementId+" #signed_form").val();
	
	var _terms			=	$(elementId+" #terms").is(":checked")?1:0;
	
	
    if (_first_name.trim() == "") {
		showAlertOnMessagePost("Please First Name",2,elementId);
    }
	else if (_last_name.trim() == "") {
		showAlertOnMessagePost("Please Last Name",2,elementId);
    }
    else if (_dob.trim() == "") {
		showAlertOnMessagePost("Please Date of Bith",2,elementId);
    }
	else if (_first_phone.trim() == "") {
		showAlertOnMessagePost("Please First phone",2,elementId);
    }
    else if (_sec_phone.trim() == "") {
		showAlertOnMessagePost("Please Last phone",2,elementId);
    }
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
    }
	else if (_gender == -1) {
		showAlertOnMessagePost("Please choose gender ",2,elementId);
    }
    else if (_maritial_status == -1) {
		showAlertOnMessagePost("Please choose maritial status",2,elementId);
    }
	else if (_aadhar_num.trim() == "") {
		showAlertOnMessagePost("Please Enter Aadhar number",2,elementId);
    }
	else if (_pancard_num.trim() == "") {
		showAlertOnMessagePost("Please Enter Pancard number",2,elementId);
    }
	else if (_address.trim() == "") {
		showAlertOnMessagePost("Please Enter Street Number",2,elementId);
    }
    else if (_currentcity == -1) {
		showAlertOnMessagePost("Please choose city",2,elementId);
    }
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Pincode",2,elementId);
    }
 
    else if (_bank == -1) {
		showAlertOnMessagePost("Please Choose Bank",2,elementId);
    }
	else if (_account_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Account Name",2,elementId);
    }
    else if (_account_num.trim() == "") {
		showAlertOnMessagePost("Please Enter Account Number",2,elementId);
    }
	else if (_ifsc_code.trim() == "") {
		showAlertOnMessagePost("Please Enter IFSC Code",2,elementId);
    }
	
	
    else if (_aadhar_file.trim() == "") {
		showAlertOnMessagePost("Please choose aadhar card image",2,elementId);
    }
	 else if (_pancard_file.trim() == "") {
		showAlertOnMessagePost("Please  choose pancard card image",2,elementId);
	}
	else if (_signed_form.trim() == "") {
		showAlertOnMessagePost("Please choose signed form image",2,elementId);
	}
	else if (_terms == 0) {
		showAlertOnMessagePost("Please Agree terms and conditions",2,elementId);
    }
    else {
		
	
		var form_data = 
		{
			first_name		:	_first_name,
			last_name 		:	_last_name,
			dob 			: 	_dob,
			first_phone 	: 	_first_phone,
			sec_phone 		: 	_sec_phone,
			email 			: 	_email,
			gender			:	_gender,
			maritial_status	:	_maritial_status,
			aadhar_num 		: 	_aadhar_num,
			pancard_num 	: 	_pancard_num,
			address 		: 	_address,
			currentcity 	: 	_currentcity,
			pincode			:	_pincode,
			bank			:	_bank,
			account_num		:	_account_num,
			account_name	:	_account_name,
			ifsc_code		:	_ifsc_code,
			aadhar_file		:	_aadhar_file,
			pancard_file	:	_pancard_file,
			signed_form		:	_signed_form
			
		};
		
		
		callService(form_data,elementId);
     
    }
  });
 

/* ///// DATA VERIFICATION ////// */


 function verifyDOB(ElementId)
 {
	var dob=$(ElementId +" #dob").val();
	
	return dob;
 }
 
 function verifyName(ElementId)
 {
	var name=$(ElementId +" #name").val();
	
	return name;
 }
 
 function verifyEmail(ElementId)
 {
	var email=$(ElementId +" #email").val();
	
	return email;
 }
 
 function verifyMobileNumber(ElementId)
 {
	var mobile=$(ElementId +" #phone").val();
	
	return mobile;
 }
 
 function verifyPincode(ElementId)
 {
	var pincode=$(ElementId +" #pincode").val();
	
	return pincode;
 }
 
 function verifyRTO(ElementId)
 {
	var rto=$(ElementId +" #rto").val();
	
	return rto;
 }

 function verify_reg_num(ElementId)
 {
	var elementVal=$(ElementId +" #reg_num").val();
	
	return elementVal;
 }

 function verifyManufecturer(ElementId)
 {
	var elementVal=$(ElementId +" #manufecturer").val();
	
	return elementVal;
 }
 
 function verify_model(ElementId)
 {
	var elementVal=$(ElementId +" #model").val();
	
	return elementVal;
 }
 
 function verify_fuel_type(ElementId)
 {
	var elementVal=$(ElementId +" #fuel_type").val();
	return elementVal;
	
 }
 
 function verify_variant(ElementId)
 {
	var elementVal=$(ElementId +" #variant").val();
	
	return elementVal;
 }
 
 function verify_reg_yr(ElementId)
 {
	var elementVal=$(ElementId +" #reg_yr").val();
	
	return elementVal;
 }
 
 function verify_insurer(ElementId)
 {
	var elementVal=$(ElementId +" #insurer").val();
	return elementVal;
	
 }
 
 
 /* ///  Service Call // */
 
 function callService(form_data,id="")
 {
	 $.post("php/merchantInfoPage.php", form_data, function(data){

		var jsonData = JSON.parse(data);
		
		if(jsonData.result == 1)
		{
			showAlertOnMessagePost("Data is posted",1,id);
		}
		else
		{
			showAlertOnMessagePost("Something went wrong on server.",2,id);
		}
		
      });
 }
 
 										
	$('#addMorePerson').on('click',function(){
		var div = $("#health_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"health_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"health_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";

		$('#health_insurance_person_details').append(singleRow);

	});
	
	$('#removePerson').on('click',function(){
	var div = $("#health_insurance_person_details");
	var child_count = div.children().length;
	var children = div.children();
	children[child_count-1].remove;
	});
	
	$('#addMoreTravelerFam').on('click',function(){
		var div = $("#travel_fam_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"travel_fam_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"travel_fam_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";
		$('#travel_fam_insurance_person_details').append(singleRow);

	});
	
	$('#addMoreTravelerGrp').on('click',function(){
		var div = $("#travel_grp_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"travel_grp_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"travel_grp_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";
		$('#travel_grp_insurance_person_details').append(singleRow);

	});
	
 
/* 	
	SUCCESS:1,
	INCOMPLETE_DATA:2,
	FAILED:3,
	NONE:4 
*/
	


  function showAlertOnMessagePost(message,msg_type,id="")
  {
	if(msg_type == 1)
	{	
		 $(id +" #success").removeClass("success_none");
      if (!$(id +" #success").hasClass("success")) {
        $(id +" #success").addClass("success");
      }
	}
	else if(msg_type == 2)
	{	
		$(id +" #success").removeClass("success_none");
		if (!$(id +" #success").hasClass("alert-display")) {
		  $(id +" #success").addClass("alert-display");
		}
	}
	else if(msg_type == 4)
	{	
		if (!$(id +" #success").hasClass("alert-display")) {
			$(id +" #success").removeClass("alert-display");
		}
		if (!$(id +" #success").hasClass("success")) {
			$(id +" #success").removeClass("success");
		}
		$(id +" #success").addClass("success_none");	
	}
	
    $(id +" #success").html(message);
  }

  
 

});
