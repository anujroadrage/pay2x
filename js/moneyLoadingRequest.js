var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';


 function onClickBuyNow()
{
	var temp= this.tag;
}


$(document).ready(function(){

 document.getElementById("moneyLoadingRequest").querySelector("#request_id").value  ="p2x"+Math.floor(Math.random() * 1000000) + 1 ;
 document.getElementById("moneyLoadingRequest").querySelector("#request_id").innerHTML=document.getElementById("moneyLoadingRequest").querySelector("#request_id").value;
/* ///// Merchant Info ///// */

$("#moneyLoadingRequest #submit").click(function(e){

	e.preventDefault();
  
  
	var elementId ="#moneyLoadingRequest";
	showAlertOnMessagePost("",4,elementId);
	
	var _request_id 	=	$(elementId+" #request_id").val();
	var _load_amount 	=	$(elementId+" #load_amount").val();
	var _payment_mode	=	$(elementId+" #payment_mode").val();
	var _ref_num		=	$(elementId+" #ref_num").val();
	var _deposit_date	=	$(elementId+" #deposit_date").val();
	var _credit_date	=	$(elementId+" #credit_date").val();
	var _cred_bank_num	=	$(elementId+" #cred_bank_num").val();
	var _remarks		=	$(elementId+" #remarks").val();
	var _screenshot		=	$(elementId+" #screenshot").val();
	

    if (_load_amount.trim() == "") {
		showAlertOnMessagePost("Please Load amount",2,elementId);
    }
	else if (_payment_mode == -1) {
		showAlertOnMessagePost("Please Select Payment Mode",2,elementId);
    }
    else if (_ref_num.trim() == "") {
		showAlertOnMessagePost("Please Enter reference number",2,elementId);
    }
	else if (_deposit_date.trim() == "") {
		showAlertOnMessagePost("Please deposit date",2,elementId);
    }
    else if (_credit_date.trim() == "") {
		showAlertOnMessagePost("Please Credit date",2,elementId);
    }
	else if (_cred_bank_num.trim() == "") {
		showAlertOnMessagePost("Please Credit bank number",2,elementId);
    }
    else {
		
	
		var form_data = 
		{
			request_id		:	_request_id,
			load_amount 	:	_load_amount,
			payment_mode 	: 	_payment_mode,
			ref_num 		: 	_ref_num,
			deposit_date 	: 	_deposit_date,
			credit_date 	: 	_credit_date,
			cred_bank_num	:	_cred_bank_num,
			remarks			:	_remarks,
			screenshot		:	_screenshot
	
		};
		
		callService(form_data,elementId);
     
    }
  });
 


 
 /* ///  Service Call // */
 
 function callService(form_data,id="")
 {
	 $.post("php/moneyLoadingRequest.php", form_data, function(data){

		var jsonData = JSON.parse(data);
		if(jsonData.result == 1)
		{
			showAlertOnMessagePost("Data is posted",1,id);
		}
		else
		{
			showAlertOnMessagePost("ERROR => "+jsonData.message,2,id);
		}
		
      });
 }
 
 										
	
 
/* 	
	SUCCESS:1,
	INCOMPLETE_DATA:2,
	FAILED:3,
	NONE:4 
*/
	


  function showAlertOnMessagePost(message,msg_type,id="")
  {
	if(msg_type == 1)
	{	
		 $(id +" #success").removeClass("success_none");
      if (!$(id +" #success").hasClass("success")) {
        $(id +" #success").addClass("success");
      }
	}
	else if(msg_type == 2)
	{	
		$(id +" #success").removeClass("success_none");
		if (!$(id +" #success").hasClass("alert-display")) {
		  $(id +" #success").addClass("alert-display");
		}
	}
	else if(msg_type == 4)
	{	
		if (!$(id +" #success").hasClass("alert-display")) {
			$(id +" #success").removeClass("alert-display");
		}
		if (!$(id +" #success").hasClass("success")) {
			$(id +" #success").removeClass("success");
		}
		$(id +" #success").addClass("success_none");	
	}
	
    $(id +" #success").html(message);
  }

  
 

});
