-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2018 at 03:52 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `table`
--

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `plan` varchar(2000) NOT NULL,
  `payout` varchar(1000) NOT NULL,
  `cover` varchar(1000) NOT NULL,
  `claim` varchar(1000) NOT NULL,
  `riders` varchar(1000) NOT NULL,
  `premium` varchar(1000) NOT NULL,
  `details` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`plan`, `payout`, `cover`, `claim`, `riders`, `premium`, `details`) VALUES
('bob.png', '4 crore', '2 years', '97%', 'Death due to accident.', '600', 'DETAILS'),
('hdfc.jpg', '5 crore', '3 year', '95%', 'Critical ilness', '500', 'DETAILS'),
('axis_bank.jpg', '5 crore', '6 year', '97%', 'Cover for spouse', '500', 'DETAILS'),
('icici.png', '2 crore', '3 year', '98%', '-Death due to accident 77,- Disability due to accident 16, - Critical illness 116, - Cover for spouse Available, Heart and Cancer Cover 327', '600', 'DETAILS');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
