-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2018 at 10:02 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `table`
--

-- --------------------------------------------------------

--
-- Table structure for table `creditcard_offer`
--

CREATE TABLE `creditcard_offer` (
  `ccoffer_id` int(255) NOT NULL,
  `card` varchar(255) NOT NULL,
  `fee` int(255) NOT NULL,
  `rewards` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `creditcard_offer`
--

INSERT INTO `creditcard_offer` (`ccoffer_id`, `card`, `fee`, `rewards`) VALUES
(1, 'creditcard.png', 5000, 'Enjoy privileges at Taj Hotels, Resorts and Palaces; Preferred Hotels & Resorts and Oberoi Hotels & Resorts.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `creditcard_offer`
--
ALTER TABLE `creditcard_offer`
  ADD PRIMARY KEY (`ccoffer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `creditcard_offer`
--
ALTER TABLE `creditcard_offer`
  MODIFY `ccoffer_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
