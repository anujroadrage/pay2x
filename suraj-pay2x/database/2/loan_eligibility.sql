-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2018 at 10:02 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `table`
--

-- --------------------------------------------------------

--
-- Table structure for table `loan_eligibility`
--

CREATE TABLE `loan_eligibility` (
  `loanoffer_id` int(255) NOT NULL,
  `bank` varchar(1000) NOT NULL,
  `interest_rate` varchar(1000) NOT NULL,
  `processing_fee` varchar(1000) NOT NULL,
  `emi` varchar(1000) NOT NULL,
  `prepayment_charge` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan_eligibility`
--

INSERT INTO `loan_eligibility` (`loanoffer_id`, `bank`, `interest_rate`, `processing_fee`, `emi`, `prepayment_charge`) VALUES
(1, 'axis_bank.jpg', '19.00%', '4,400', '2,000', '4% of outstanding loan\r\nNot allowed in 1st year\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `loan_eligibility`
--
ALTER TABLE `loan_eligibility`
  ADD PRIMARY KEY (`loanoffer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loan_eligibility`
--
ALTER TABLE `loan_eligibility`
  MODIFY `loanoffer_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
