$(document).ready(function(){

	$("#btn-signup").click(function(){
		var email 		= $("#signup-email").val();
		var username 	= $("#signup-username").val();
		var firstname 	= $("#signup-firstname").val();
		var lastname 	= $("#signup-lastname").val();
		var password 	= $("#signup-password").val();
		var repassword 	= $("#signup-repassword").val();
		var captcha 	= $("#signup-captcha").val();
		password = password.trim();

		//for password validation
		var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

		if (email.trim() == "" &&
			username.trim() == "" &&
			firstname.trim() == "" &&
			lastname.trim() == "" &&
			password.trim() == "" &&
			repassword.trim() == "" &&
			captcha.trim() == "") {
			  $("#signupalert").removeClass("alert-signup");
			  if (!$("#signupalert").hasClass("alert-signup")) {
				$("#signupalert").addClass("alert-signup-1");
			  }
			  $("#signupalert").html("All fields are required");
		}
		else if (email.trim() == "") {
		  $("#signupalert").removeClass("alert-signup");
		  if (!$("#signupalert").hasClass("alert-signup")) {
			$("#signupalert").addClass("alert-signup-1");
		  }
		  $("#signupalert").html("Email cannot be empty");
		}
		else if (username.trim() == "") {
		  $("#signupalert").removeClass("alert-signup");
		  if (!$("#signupalert").hasClass("alert-signup")) {
			$("#signupalert").addClass("alert-signup-1");
		  }
		  $("#signupalert").html("Username cannot be empty");
		}
		else if (firstname.trim() == "") {
		  $("#signupalert").removeClass("alert-signup");
		  if (!$("#signupalert").hasClass("alert-signup")) {
			$("#signupalert").addClass("alert-signup-1");
		  }
		  $("#signupalert").html("First Name cannot be empty");
		}
		else if (lastname.trim() == "") {
		  $("#signupalert").removeClass("alert-signup");
		  if (!$("#signupalert").hasClass("alert-signup")) {
			$("#signupalert").addClass("alert-signup-1");
		  }
		  $("#signupalert").html("Last Name cannot be empty");
		}
		else if (password.trim() == "") {
		  $("#signupalert").removeClass("alert-signup");
		  if (!$("#signupalert").hasClass("alert-signup")) {
			$("#signupalert").addClass("alert-signup-1");
		  }
		  $("#signupalert").html("Password cannot be empty");
		}
		else if (repassword.trim() == "") {
		  $("#signupalert").removeClass("alert-signup");
		  if (!$("#signupalert").hasClass("alert-signup")) {
			$("#signupalert").addClass("alert-signup-1");
		  }
		  $("#signupalert").html("Retype Password cannot be empty");
		}
		else if (captcha.trim() == "") {
		  $("#signupalert").removeClass("alert-signup");
		  if (!$("#signupalert").hasClass("alert-signup")) {
			$("#signupalert").addClass("alert-signup-1");
		  }
		  $("#signupalert").html("Captcha cannot be empty");
		}
		else {

		  //password validation
		  if (!password.match(decimal)) {
			$("#signupalert").removeClass("alert-signup");
			if (!$("#signupalert").hasClass("alert-signup")) {
			  $("#signupalert").addClass("alert-signup-1");
			}
			$("#signupalert").html("Password must be between 8 to 15 characters which" +
			"contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character");
		  }
		  else {
			$.post("php/signup.php", $("#signupform :input").serializeArray(), function(data){
			  if (data ==  "Registration Successful") {
				location.reload();
			  }
			  else if (data == "Invalid email") {
				$("#signupalert").removeClass("alert-signup");
				if (!$("#signupalert").hasClass("alert-signup")) {
				  $("#signupalert").addClass("alert-signup-1");
				}
				$("#signupalert").html("Not a valid E-mail address");
			  }
			  else if (data == "This username or email is already registered") {
				$("#signupalert").removeClass("alert-signup");
				if (!$("#signupalert").hasClass("alert-signup")) {
				  $("#signupalert").addClass("alert-signup-1");
				}
				$("#signupalert").html("This username or email is already registered");
			  }
			  else if (data == "Password and Retype password do not match") {
				$("#signupalert").removeClass("alert-signup");
				if (!$("#signupalert").hasClass("alert-signup")) {
				  $("#signupalert").addClass("alert-signup-1");
				}
				$("#signupalert").html("Password and Retype password do not match");
			  }
			  else if (data == "Incorrect Captcha") {
				$("#signupalert").removeClass("alert-signup");
				if (!$("#signupalert").hasClass("alert-signup")) {
				  $("#signupalert").addClass("alert-signup-1");
				}
				$("#signupalert").html("Incorrect Captcha");
			  }
			});
		  }
		}
	});
  
  });