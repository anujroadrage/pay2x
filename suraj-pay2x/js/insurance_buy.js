var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';

 //checking if user is already login or not
 function loginCheckStatus(){
   $(document).ready(function(){
     $.post("php/logincheck.php", function(data){
       var myobj = JSON.parse(data);

       //checking cookies whether these are set or not
       if (myobj.cookie_name != null && myobj.cookie_password != null) {
         if (myobj.cookie_name != "" && myobj.cookie_password != "") {
           $("#login-username").attr("value", myobj.cookie_name);
           $("#login-password").attr("value", myobj.cookie_password);
         }
       }

       if (myobj.status == 1) {

         //checking secure_code
         if (myobj.secure_code != 0) {
           window.location.href = "php/validate.php";
         }

         $("#username").html(myobj.username);
         $(".login").css("display", "none");
         $("#nav-1").css("display", "none");
         $("#nav-2").css("display", "block");
       }
       else if (myobj.status == 0) {
         $("#nav-2").css("display", "none");
         $(".login").css("display", "block");
         $("#nav-1").css("display", "block");
         if (myobj.modal == 1) {
           $("#myModal").modal("show");
         }
       }
     });
   });
 }

 function onClickBuyNow()
{
	var temp= this.tag;
}


$(document).ready(function(){

  //loging in user
  $("#btnlogin").click(function(){
    var username = $("#login-username").val();
    var password = $('#login-password').val();
    if (username.trim() == "" && password.trim() == "") {
      $("#login-alert").removeClass("alert-disp");
      if (!($("#login-alert").hasClass("alert-disp-1"))) {
        $("#login-alert").addClass("alert-disp-1");
      }
      $("#login-alert").html("Username or Password cannot be empty!");
    }
    else if (username.trim() == "") {
      $("#login-alert").removeClass("alert-disp");
      if (!($("#login-alert").hasClass("alert-disp-1"))) {
        $("#login-alert").addClass("alert-disp-1");
      }
      $("#login-alert").html("Username cannot be empty!");
    }
    else if (password.trim() == "") {
      $("#login-alert").removeClass("alert-disp");
      if (!($("#login-alert").hasClass("alert-disp-1"))) {
        $("#login-alert").addClass("alert-disp-1");
      }
      $("#login-alert").html("Password cannot be empty!");
    }
    else {
      $.post("php/login.php", $("#loginform :input").serializeArray(), function(data){
        if(data == "Login Successful"){
          location.reload();
        }
        else if (data == "Incorrect Username or Password") {
          $("#login-alert").removeClass("alert-disp");
          if (!($("#login-alert").hasClass("alert-disp-1"))) {
            $("#login-alert").addClass("alert-disp-1");
          }
          $("#login-alert").html(data);
        }
      });
    }
  });

  //sign up
  $("#btn-signup").click(function(){
    var email = $("#signup-email").val();
    var username = $("#signup-username").val();
    var firstname = $("#signup-firstname").val();
    var lastname = $("#signup-lastname").val();
    var password = $("#signup-password").val();
    var repassword = $("#signup-repassword").val();
    var captcha = $("#signup-captcha").val();
    password = password.trim();

    //for password validation
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

    if (email.trim() == "" &&
        username.trim() == "" &&
        firstname.trim() == "" &&
        lastname.trim() == "" &&
        password.trim() == "" &&
        repassword.trim() == "" &&
        captcha.trim() == "") {
          $("#signupalert").removeClass("alert-signup");
          if (!$("#signupalert").hasClass("alert-signup")) {
            $("#signupalert").addClass("alert-signup-1");
          }
          $("#signupalert").html("All fields are required");
    }
    else if (email.trim() == "") {
      $("#signupalert").removeClass("alert-signup");
      if (!$("#signupalert").hasClass("alert-signup")) {
        $("#signupalert").addClass("alert-signup-1");
      }
      $("#signupalert").html("Email cannot be empty");
    }
    else if (username.trim() == "") {
      $("#signupalert").removeClass("alert-signup");
      if (!$("#signupalert").hasClass("alert-signup")) {
        $("#signupalert").addClass("alert-signup-1");
      }
      $("#signupalert").html("Username cannot be empty");
    }
    else if (firstname.trim() == "") {
      $("#signupalert").removeClass("alert-signup");
      if (!$("#signupalert").hasClass("alert-signup")) {
        $("#signupalert").addClass("alert-signup-1");
      }
      $("#signupalert").html("First Name cannot be empty");
    }
    else if (lastname.trim() == "") {
      $("#signupalert").removeClass("alert-signup");
      if (!$("#signupalert").hasClass("alert-signup")) {
        $("#signupalert").addClass("alert-signup-1");
      }
      $("#signupalert").html("Last Name cannot be empty");
    }
    else if (password.trim() == "") {
      $("#signupalert").removeClass("alert-signup");
      if (!$("#signupalert").hasClass("alert-signup")) {
        $("#signupalert").addClass("alert-signup-1");
      }
      $("#signupalert").html("Password cannot be empty");
    }
    else if (repassword.trim() == "") {
      $("#signupalert").removeClass("alert-signup");
      if (!$("#signupalert").hasClass("alert-signup")) {
        $("#signupalert").addClass("alert-signup-1");
      }
      $("#signupalert").html("Retype Password cannot be empty");
    }
    else if (captcha.trim() == "") {
      $("#signupalert").removeClass("alert-signup");
      if (!$("#signupalert").hasClass("alert-signup")) {
        $("#signupalert").addClass("alert-signup-1");
      }
      $("#signupalert").html("Captcha cannot be empty");
    }
    else {

      //password validation
      if (!password.match(decimal)) {
        $("#signupalert").removeClass("alert-signup");
        if (!$("#signupalert").hasClass("alert-signup")) {
          $("#signupalert").addClass("alert-signup-1");
        }
        $("#signupalert").html("Password must be between 8 to 15 characters which" +
        "contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character");
      }
      else {
        $.post("php/signup.php", $("#signupform :input").serializeArray(), function(data){
          if (data ==  "Registration Successful") {
            location.reload();
          }
          else if (data == "Invalid email") {
            $("#signupalert").removeClass("alert-signup");
            if (!$("#signupalert").hasClass("alert-signup")) {
              $("#signupalert").addClass("alert-signup-1");
            }
            $("#signupalert").html("Not a valid E-mail address");
          }
          else if (data == "This username or email is already registered") {
            $("#signupalert").removeClass("alert-signup");
            if (!$("#signupalert").hasClass("alert-signup")) {
              $("#signupalert").addClass("alert-signup-1");
            }
            $("#signupalert").html("This username or email is already registered");
          }
          else if (data == "Password and Retype password do not match") {
            $("#signupalert").removeClass("alert-signup");
            if (!$("#signupalert").hasClass("alert-signup")) {
              $("#signupalert").addClass("alert-signup-1");
            }
            $("#signupalert").html("Password and Retype password do not match");
          }
          else if (data == "Incorrect Captcha") {
            $("#signupalert").removeClass("alert-signup");
            if (!$("#signupalert").hasClass("alert-signup")) {
              $("#signupalert").addClass("alert-signup-1");
            }
            $("#signupalert").html("Incorrect Captcha");
          }
        });
      }
    }
  });

  //contact us
  $("#btnContactUs").click(function(){
    //getting values of the fields
    var name = $("#contact-us-name").val();
    var email = $("#contact-us-email").val();
    var subject = $("#contact-us-subject").val();
    var message = $("#contact-us-message").val();
    name = name.trim();
    email = email.trim();
    message = message.trim();

    if (name == "" && email == "" && subject == null && message == "") {
      alert("All fields are required");
    }
    else if (name == "") {
      alert("Name cannot be empty");
    }
    else if (email == "") {
      alert("E-mail cannot be empty");
    }
    else if (subject == null) {
      alert("Subject cannot be empty");
    }
    else if (message == "") {
      alert("Message cannot be empty");
    }
    else {
      $.post("php/contact.php",
      {
        name: name,
        e_mail: email,
        subject: subject,
        message: message
      },
      function(data){
        if (data == "We will contact you soon via E-mail") {
          alert(data);
        }
        else {
          alert(data);
        }
      });
    }
  });

  //loading testimonials
  $.post("php/insuranceBuy.php",
      {
        tag: 'insurancebuy'

      },
	  function(data) {
    var myobj = JSON.parse(data);
	
	var tableBody = document.getElementById('plan_table_body');
	
	
	
	var tr = document.createElement('tr');
	tr.id = "plan_table_body_tr";
		
	tableBody.appendChild(tr);
	var rowString = "<td ><img src=\"images/"+myobj.plan+"\"></td>"+
	"<td>"+myobj.payout+"</td>"+
	"<td>"+myobj.cover+"</td>"+
	"<td>"+myobj.claim+"</td>"+
	"<td>"+myobj.riders+"</td>"+
	"<td>"+myobj.premium+"</td>";
	
 
 	
	tr.innerHTML=rowString;
	
	
	

  });
});
