var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';

 //checking if user is already login or not
 function loginCheckStatus(){
   $(document).ready(function(){
     $.post("php/logincheck.php", function(data){
       var myobj = JSON.parse(data);

       //checking cookies whether these are set or not
       if (myobj.cookie_name != null && myobj.cookie_password != null) {
         if (myobj.cookie_name != "" && myobj.cookie_password != "") {
           $("#login-username").attr("value", myobj.cookie_name);
           $("#login-password").attr("value", myobj.cookie_password);
         }
       }

       if (myobj.status == 1) {
         //checking secure_code
         if (myobj.secure_code != 0) {
           window.location.href = "php/validate.php";
         }

         $("#username").html(myobj.username);
         $(".login").css("display", "none");
         $("#nav-1").css("display", "none");
         $("#nav-2").css("display", "block");
       }
       else if (myobj.status == 0) {
         $("#nav-2").css("display", "none");
         $(".login").css("display", "block");
         $("#nav-1").css("display", "block");
         if (myobj.modal == 1) {
           $("#myModal").modal("show");
         }
       }
     });
   });
 }

 function onClickBuyNow()
{
	var temp= this.tag;
}


$(document).ready(function(){


/* ///// TERM INSURANCE ///// */

$("#term_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#term_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	var _amount =	$(elementId+" #amount").val();
	var _dob	= 	verifyDOB(elementId);
	var _smoke	= 	$(elementId+" #smoke").val();
	var _income	=	$(elementId+" #income").val();

	var _name 	= 	verifyName(elementId);
	var _email	=  	verifyEmail(elementId);
	var _phone	= 	verifyMobileNumber(elementId);
	var _pincode= 	verifyPincode(elementId);
	
    if (_amount == -1) {
		showAlertOnMessagePost("Please Enter amount",2,elementId);
    }
    else if (_dob.trim() == "") {
		showAlertOnMessagePost("Please Enter Date Of Birth",2,elementId);
    }
	 else if (_income == -1) {
		showAlertOnMessagePost("Please Enter Income",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			amount 				: 	_amount,
			dob 				: 	_dob,
			smoke 				: 	_smoke,
			income 				: 	_income,
			name 				: 	_name,
			email 				: 	_email,
			phone 				: 	_phone,
			pincode 			: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
     
    }
  });
 
  
/* ///// HEALTH INSURANCE ///// */

$("#health_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#health_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $("#health_insurance_contactForm #service_name").val();
	var _sub_service_name =  $("#health_insurance_contactForm #sub_service_name").val();
	
	
	var _income	=	$("#health_insurance_contactForm #income").val();
	
	var _name 	= 	verifyName(elementId);
	var _email	=  	verifyEmail(elementId);
	var _phone	= 	verifyMobileNumber(elementId);
	var _pincode= 	verifyPincode(elementId);
	var _memberInfo = {};
	var div = $("#health_insurance_person_details");
	var child_count = div.children().length;
	var children = div.children();
	
	for(var idx =0; idx<child_count; idx++)
	{
		var info = children[idx].getElementsByTagName("input");
		_memberInfo["age"+idx] = 	info["age"].value;
		
	}
	
	
	
	if (_memberInfo["age0"] == "") {
		showAlertOnMessagePost("Please Enter Atlease one member info",2,elementId);
	}
	else if (_income == -1) {
		showAlertOnMessagePost("Please Enter Income",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			income 				: 	_income,
			member_info			:JSON.stringify(_memberInfo),
			name 				: 	_name,
			//data 				: 	_stringData,
			email 				: 	_email,
			phone 				: 	_phone,
			pincode 			: 	_pincode
			
		};
		
		callService(form_data,elementId);
     
    }
  });
   
/* ///// CAR INSURANCE ///// */


$("#car_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#car_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	var _rto 			= 	verifyRTO(elementId);
	var _reg_num 		= 	verify_reg_num(elementId);
	var _manufecturer 	= 	verifyManufecturer(elementId);
	var _model 			= 	verify_model(elementId);
	var _fuel_type 		= 	verify_fuel_type(elementId);
	var _variant 		= 	verify_variant(elementId);
	var _reg_yr 		= 	verify_reg_yr(elementId);
	var _insurer 		= 	verify_insurer(elementId);
	
	var _name 			= 	verifyName(elementId);
	var _email			=  	verifyEmail(elementId);
	var _phone			= 	verifyMobileNumber(elementId);
	var _pincode		= 	verifyPincode(elementId);
	
    if (_reg_num.trim() == "") {
		showAlertOnMessagePost("Please Enter Reg. num (e.g. : DL-8-CN-8842)",2,elementId);
    }
    else if (_rto.trim() == "") {
		showAlertOnMessagePost("Please Enter RTO",2,elementId);
    }
    else if (_manufecturer.trim() == "") {
		showAlertOnMessagePost("Please Enter Manufecturer",2,elementId);
    }
    else if (_model.trim() == "") {
		showAlertOnMessagePost("Please Enter Model",2,elementId);
    }
    else if (_fuel_type == -1) {
		showAlertOnMessagePost("Please Enter Fuel Type",2,elementId);
    }
    else if (_variant.trim() == "") {
		showAlertOnMessagePost("Please Enter Variant",2,elementId);
    }
    else if (_reg_yr.trim() == "") {
		showAlertOnMessagePost("Please Enter Registration Year",2,elementId);
    }
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			rto 				: 	_rto,
			reg_num 			: 	_reg_num,
			manufecturer		: 	_manufecturer,
			model 				: 	_model,
			fuel_type			: 	_fuel_type,
			variant				:	_variant,
			reg_yr				:	_reg_yr,
			insurer				:	_insurer,
			name				:	_name,
			phone 				: 	_phone,
			email 				: 	_email,
			pincode 			: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
    }
     
  });
  

/* ///// BIKE INSURANCE ///// */
$("#bike_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#bike_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	var _rto 			= 	verifyRTO(elementId);
	var _reg_num 		= 	verify_reg_num(elementId);
	var _manufecturer 	= 	verifyManufecturer(elementId);
	var _model 			= 	verify_model(elementId);
	var _fuel_type 		=	"-2";
	var _variant 		= 	verify_variant(elementId);
	var _reg_yr 		= 	verify_reg_yr(elementId);
	var _insurer 		= 	verify_insurer(elementId);
	
	var _name 			= 	verifyName(elementId);
	var _email			=  	verifyEmail(elementId);
	var _phone			= 	verifyMobileNumber(elementId);
	var _pincode		= 	verifyPincode(elementId);
	
    if (_reg_num.trim() == "") {
		showAlertOnMessagePost("Please Enter Reg. num (e.g. : DL-8-CN-8842)",2,elementId);
    }
    else if (_rto.trim() == "") {
		showAlertOnMessagePost("Please Enter RTO",2,elementId);
    }
    else if (_manufecturer.trim() == "") {
		showAlertOnMessagePost("Please Enter Manufecturer",2,elementId);
    }
    else if (_model.trim() == "") {
		showAlertOnMessagePost("Please Enter Model",2,elementId);
    }
    else if (_fuel_type == -1) {
		showAlertOnMessagePost("Please Enter Fuel Type",2,elementId);
    }
    else if (_variant.trim() == "") {
		showAlertOnMessagePost("Please Enter Variant",2,elementId);
    }
    else if (_reg_yr.trim() == "") {
		showAlertOnMessagePost("Please Enter Registration Year",2,elementId);
    }
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			rto 				: 	_rto,
			reg_num 			: 	_reg_num,
			manufecturer		: 	_manufecturer,
			model 				: 	_model,
			fuel_type			: 	_fuel_type,
			variant				:	_variant,
			reg_yr				:	_reg_yr,
			insurer				:	_insurer,
			name				:	_name,
			phone 				: 	_phone,
			email 				: 	_email,
			pincode 			: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
    }
     
  });

  
/* ///// HEART INSURANCE ///// */
$("#heart_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#heart_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	var _amount =	$(elementId+" #amount").val();
	var _dob	= 	verifyDOB(elementId);
	var _smoke	= 	$(elementId+" #smoke").val();
	var _income	=	$(elementId+" #income").val();

	var _name 	= 	verifyName(elementId);
	var _email	=  	verifyEmail(elementId);
	var _phone	= 	verifyMobileNumber(elementId);
	var _pincode= 	verifyPincode(elementId);
	
    if (_amount == -1) {
		showAlertOnMessagePost("Please Enter amount",2,elementId);
    }
    else if (_dob.trim() == "") {
		showAlertOnMessagePost("Please Enter Date Of Birth",2,elementId);
    }
	 else if (_income == -1) {
		showAlertOnMessagePost("Please Enter Income",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			amount 				: 	_amount,
			dob 				: 	_dob,
			smoke 				: 	_smoke,
			income 				: 	_income,
			name 				: 	_name,
			email 				: 	_email,
			phone 				: 	_phone,
			pincode 			: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
     
    }
  });
  

/* ///// CANCER INSURANCE ///// */

$("#cancer_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#cancer_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	var _amount =	$(elementId+" #amount").val();
	var _dob	= 	verifyDOB(elementId);
	var _smoke	= 	$(elementId+" #smoke").val();
	var _income	=	$(elementId+" #income").val();

	var _name 	= 	verifyName(elementId);
	var _email	=  	verifyEmail(elementId);
	var _phone	= 	verifyMobileNumber(elementId);
	var _pincode= 	verifyPincode(elementId);
	
    if (_amount == -1) {
		showAlertOnMessagePost("Please Enter amount",2,elementId);
    }
    else if (_dob.trim() == "") {
		showAlertOnMessagePost("Please Enter Date Of Birth",2,elementId);
    }
	 else if (_income == -1) {
		showAlertOnMessagePost("Please Enter Income",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			amount 				: 	_amount,
			dob 				: 	_dob,
			smoke 				: 	_smoke,
			income 				: 	_income,
			name 				: 	_name,
			email 				: 	_email,
			phone 				: 	_phone,
			pincode 			: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
     
    }
  });
 
/* ///// PROPERTY INSURANCE ///// */

$("#property_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#property_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	
	var _property_cat 		=$(elementId+" #property_cat").val();
	var _property_owner 	=$(elementId+" #property_owner").val();
	var _age 				=$(elementId+" #age").val();
	var _property_type 		=$(elementId+" #property_type").val();
	var _risc_covered 		=$(elementId+" #risc_covered").val();
	var _sum 				=$(elementId+" #sum").val();
	var _furniture 			=$(elementId+" #furniture").val();
	var _elec_break 		=$(elementId+" #elec_break").val();
	var _elec_app_break 	=$(elementId+" #elec_app_break").val();
	var _fire 				=$(elementId+" #fire").val();
	var _burglary 			=$(elementId+" #burglary").val();
	var _secured_measures 	=$(elementId+" #secured_measures").val();
	
	var _name 				= 	verifyName(elementId);
	var _email				=  	verifyEmail(elementId);
	var _phone				= 	verifyMobileNumber(elementId);
	var _pincode			= 	verifyPincode(elementId);
	
    if (_property_cat == -1) {
		showAlertOnMessagePost("Please Select Property Category",2,elementId);
    }
    else if (_property_owner == -1) {
		showAlertOnMessagePost("Please Select Property Owner",2,elementId);
    }
    else  if (_age == "") {
		showAlertOnMessagePost("Please Enter Age of Property",2,elementId);
    }
    else  if (_property_type == -1) {
		showAlertOnMessagePost("Please Select Property Type",2,elementId);
    }
	else  if (_risc_covered == -1) {
		showAlertOnMessagePost("Please Select Risc to be Covered",2,elementId);
    }
    else  if (_sum == -1) {
		showAlertOnMessagePost("Please Enter Sum to be insured",2,elementId);
    }
    else  if (_furniture == -1) {
		showAlertOnMessagePost("Please Enter Furniture",2,elementId);
    }
    else  if (_elec_break == -1) {
		showAlertOnMessagePost("Please Enter Electricity broken to be insured",2,elementId);
    }
	else  if (_elec_app_break == -1) {
		showAlertOnMessagePost("Please Enter Electricity Appliances broken to be insured",2,elementId);
    }
	else  if (_fire == -1) {
		showAlertOnMessagePost("Please Enter Sum insured due to fire",2,elementId);
    }
	else  if (_secured_measures == -1) {
		showAlertOnMessagePost("Please Select secured measures",2,elementId);
    }
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
	
		var form_data = 
		{
			service_name			:	_service_name,
			sub_service_name 		:	_sub_service_name,
			property_cat			:	_property_cat,
			property_owner 			: 	_property_owner,
			age 					: 	_age,
			property_type 			: 	_property_type,
			risc_covered 			: 	_risc_covered,
			sum						:	_sum,
			furniture				:	_furniture,
			elec_break				:	_elec_break,
			elec_app_break			:	_elec_app_break ,
			fire					:	_fire,
			burglary				:	_burglary,
			secured_measures		:	_secured_measures,
			name 					: 	_name,
			email 					: 	_email,
			phone 					: 	_phone,
			pincode 				: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
     
    }
  });

/* ///// TRAVEL INSURANCE (FAMILY)///// */

$("#trav_fam_insurance_contactForm #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#trav_fam_insurance_contactForm";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	var _dest_country =	$(elementId+" #dest_country").val();
	var _memberInfo ={};
	var _startDate	= 	$(elementId+" #startDate").val();
	var _endDate	= 	$(elementId+" #EndDate").val();
	
	var _name 	= 	verifyName(elementId);
	var _email	=  	verifyEmail(elementId);
	var _phone	= 	verifyMobileNumber(elementId);
	var _pincode= 	verifyPincode(elementId);
	
	var div = $("#travel_fam_insurance_person_details");
	var child_count = div.children().length;
	var children = div.children();
	
	for(var idx =0; idx<child_count; idx++)
	{
		var info = children[idx].getElementsByTagName("input");
		_memberInfo["age"+idx] = 	info["age"].value;
		
	}
	
    if (_dest_country == -1) {
		showAlertOnMessagePost("Please select destination country",2,elementId);
    }
    else if (_memberInfo["age0"] == "") {
		showAlertOnMessagePost("Please Enter atleast one member info",2,elementId);
    }
	else if (_startDate == "") {
		showAlertOnMessagePost("Please Enter Start Date",2,elementId);
	}
	else if (_endDate == "") {
		showAlertOnMessagePost("Please Enter End Date",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			dest_country		:	_dest_country,
			member_info			:	JSON.stringify(_memberInfo),
			start_date			:	_startDate,
			end_date 			:	_endDate,
			name 				: 	_name,
			email 				: 	_email,
			phone 				: 	_phone,
			pincode 			: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
     
    }
  });
  
 
/* ///// TRAVEL INSURANCE (GROUP)///// */
 
$("#travel_insurance_group_form #submit").click(function(e){

	e.preventDefault();
  
	var elementId ="#travel_insurance_group_form";
	showAlertOnMessagePost("",4,elementId);
	
	var _service_name =  $(elementId+" #service_name").val();
	var _sub_service_name =  $(elementId+" #sub_service_name").val();
	
	var _dest_country 	=	$(elementId+" #dest_country").val();
	var _traveler_type = 	$(elementId+" #traveler_type").val();
	var _memberInfo 	=	{};
	var _startDate		= 	$(elementId+" #startDate").val();
	var _endDate		= 	$(elementId+" #EndDate").val();
	
	var _name 	= 	verifyName(elementId);
	var _email	=  	verifyEmail(elementId);
	var _phone	= 	verifyMobileNumber(elementId);
	var _pincode= 	verifyPincode(elementId);
	
	var div = $("#travel_grp_insurance_person_details");
	var child_count = div.children().length;
	var children = div.children();
	
	for(var idx =0; idx<child_count; idx++)
	{
		var info = children[idx].getElementsByTagName("input");
		_memberInfo["age"+idx] = 	info["age"].value;
		
	}
	
    if (_dest_country == -1) {
		showAlertOnMessagePost("Please select destination country",2,elementId);
    }
    else if (_traveler_type == -1) {
		showAlertOnMessagePost("Please select Traveller Type",2,elementId);
    }
    else if (_memberInfo["age0"] == "") {
		showAlertOnMessagePost("Please Enter atleast one member info",2,elementId);
    }
	else if (_startDate == "") {
		showAlertOnMessagePost("Please Enter Start Date",2,elementId);
	}
	else if (_endDate == "") {
		showAlertOnMessagePost("Please Enter End Date",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {
		
		var form_data = 
		{
			service_name		:	_service_name,
			sub_service_name 	:	_sub_service_name,
			dest_country		:	_dest_country,
			traveler_type		:	_traveler_type,
			member_info			:	JSON.stringify(_memberInfo),
			start_date			:	_startDate,
			end_date 			:	_endDate,
			name 				: 	_name,
			email 				: 	_email,
			phone 				: 	_phone,
			pincode 			: 	_pincode
			
		};
		
		
		callService(form_data,elementId);
     
    }
  });
   
 
/* ///// DATA VERIFICATION ////// */


 function verifyDOB(ElementId)
 {
	var dob=$(ElementId +" #dob").val();
	
	return dob;
 }
 
 function verifyName(ElementId)
 {
	var name=$(ElementId +" #name").val();
	
	return name;
 }
 
 function verifyEmail(ElementId)
 {
	var email=$(ElementId +" #email").val();
	
	return email;
 }
 
 function verifyMobileNumber(ElementId)
 {
	var mobile=$(ElementId +" #phone").val();
	
	return mobile;
 }
 
 function verifyPincode(ElementId)
 {
	var pincode=$(ElementId +" #pincode").val();
	
	return pincode;
 }
 
 function verifyRTO(ElementId)
 {
	var rto=$(ElementId +" #rto").val();
	
	return rto;
 }

 function verify_reg_num(ElementId)
 {
	var elementVal=$(ElementId +" #reg_num").val();
	
	return elementVal;
 }

 function verifyManufecturer(ElementId)
 {
	var elementVal=$(ElementId +" #manufecturer").val();
	
	return elementVal;
 }
 
 function verify_model(ElementId)
 {
	var elementVal=$(ElementId +" #model").val();
	
	return elementVal;
 }
 
 function verify_fuel_type(ElementId)
 {
	var elementVal=$(ElementId +" #fuel_type").val();
	return elementVal;
	
 }
 
 function verify_variant(ElementId)
 {
	var elementVal=$(ElementId +" #variant").val();
	
	return elementVal;
 }
 
 function verify_reg_yr(ElementId)
 {
	var elementVal=$(ElementId +" #reg_yr").val();
	
	return elementVal;
 }
 
 function verify_insurer(ElementId)
 {
	var elementVal=$(ElementId +" #insurer").val();
	return elementVal;
	
 }
 
 
 /* ///  Service Call // */
 
 function callService(form_data,id="")
 {
	 $.post("suraj-pay2x/php/insuranceFirstForm.php", form_data, function(data){
        
		showAlertOnMessagePost("Data is posted",1,id);
		
		var jsonData = JSON.parse(data);
		
		if(jsonData.result == 1)
		{
			window.location.href = jsonData.url;
		}
		
		if(data == "Login Successful"){
          location.reload();
        }
		
        else if (data == "Incorrect Username or Password") {
          
		  $("#success").removeClass("success_none");
          if (!($("#success").hasClass("alert-display"))) {
            $("#success").addClass("alert-display");
          }
          $("#success").html(data);
		  
        }
      });
 }
 
 										
	$('#addMorePerson').on('click',function(){
		var div = $("#health_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"health_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"health_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";

		$('#health_insurance_person_details').append(singleRow);

	});
	
	$('#removePerson').on('click',function(){
	var div = $("#health_insurance_person_details");
	var child_count = div.children().length;
	var children = div.children();
	children[child_count-1].remove;
	});
	
	$('#addMoreTravelerFam').on('click',function(){
		var div = $("#travel_fam_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"travel_fam_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"travel_fam_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";
		$('#travel_fam_insurance_person_details').append(singleRow);

	});
	
	$('#addMoreTravelerGrp').on('click',function(){
		var div = $("#travel_grp_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"travel_grp_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"travel_grp_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";
		$('#travel_grp_insurance_person_details').append(singleRow);

	});
	
 
/* 	
	SUCCESS:1,
	INCOMPLETE_DATA:2,
	FAILED:3,
	NONE:4 
*/
	


  function showAlertOnMessagePost(message,msg_type,id="")
  {
	if(msg_type == 1)
	{	
		 $(id +" #success").removeClass("success_none");
      if (!$(id +" #success").hasClass("success")) {
        $(id +" #success").addClass("success");
      }
	}
	else if(msg_type == 2)
	{	
		$(id +" #success").removeClass("success_none");
		if (!$(id +" #success").hasClass("alert-display")) {
		  $(id +" #success").addClass("alert-display");
		}
	}
	else if(msg_type == 4)
	{	
		if (!$(id +" #success").hasClass("alert-display")) {
			$(id +" #success").removeClass("alert-display");
		}
		if (!$(id +" #success").hasClass("success")) {
			$(id +" #success").removeClass("success");
		}
		$(id +" #success").addClass("success_none");	
	}
	
    $(id +" #success").html(message);
  }

  
 

});
