var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';

 //checking if user is already login or not
 function loginCheckStatus(){
   $(document).ready(function(){
     $.post("php/logincheck.php", function(data){
       var myobj = JSON.parse(data);

       //checking cookies whether these are set or not
       if (myobj.cookie_name != null && myobj.cookie_password != null) {
         if (myobj.cookie_name != "" && myobj.cookie_password != "") {
           $("#login-username").attr("value", myobj.cookie_name);
           $("#login-password").attr("value", myobj.cookie_password);
         }
       }

       if (myobj.status == 1) {

         //checking secure_code
         if (myobj.secure_code != 0) {
           window.location.href = "php/validate.php";
         }

         $("#username").html(myobj.username);
         $(".login").css("display", "none");
         $("#nav-1").css("display", "none");
         $("#nav-2").css("display", "block");
       }
       else if (myobj.status == 0) {
         $("#nav-2").css("display", "none");
         $(".login").css("display", "block");
         $("#nav-1").css("display", "block");
         if (myobj.modal == 1) {
           $("#myModal").modal("show");
         }
       }
     });
   });
 }

 


$(document).ready(function(){

  //loading testimonials
  $.post("php/loan.php", function(data) {
	  
	try{
		myobj = JSON.parse(data);
		var len = myobj.length;
		var tableBody = document.getElementById('loan_table_body');
		
		
		for(var idx=0; idx<len; idx++ )
		{
		var tr = document.createElement('tr');
		tr.id = "loan_table_body_tr"+idx;
		
		tableBody.appendChild(tr);
		
		var rowString = "<td ><img src=\"images/"+myobj[idx].bank+"\"></td>"+
		"<td>"+myobj[idx].interest_rate+"</td>"+
		"<td>"+myobj[idx].processing_fee+"</td>"+
		"<td>"+myobj[idx].emi+"</td>"+
		"<td>"+myobj[idx].prepayment_charge+"</td>"+
		"<td></br><center><a href=\"php/LoanBuy.php?id="+myobj[idx].loanoffer_id+"&btntag=details\" class=\"submit btn btn-primary\" >Details</a></center>"+
		"</br><center><a href=\"php/LoanBuy.php?id="+myobj[idx].loanoffer_id+"&btntag=buynow\" class=\"submit btn btn-primary\" >BUY NOW</a></center></td>";
		
		
		tr.innerHTML=rowString;
		
		}
	}
	catch(e)
	{
	  alert('data is not in json format');
	}
	  
	
	
   
  });
});
