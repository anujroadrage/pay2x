var BASE_PATH = '';

if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
 BASE_PATH='/sourcecode/changes_himalaya';

          var obj=document.getElementById("employment_type");
          obj.onclick=function()
                                {

                                  if((obj.selectedIndex==1)||(obj.selectedIndex==2))
                                    {
                                      $("#salaried-details").css("display","block");
                                      $("#professional-details").css("display","block");
                                      $("#prof_xp").css("display","block");
                                      $("#annual_income").css("display","block");
                                      $("#business-owner-details").css("display","none");
                                      $("#annual_turnover").css("display","none");
                                      $("#bo_xp").css("display","none");
                                      $("#bo_annual_income").css("display","none");
                                      $("#office_residence_type_id").css("display","none");
                                      $("#nature_of_business").css("display","none");
                                    }
                                  if(obj.selectedIndex==3)
                                  {
                                      $("#annual_income").css("display","block");
                                      $("#business-owner-details").css("display","block");
                                      $("#annual_turnover").css("display","block");
                                      $("#bo_xp").css("display","block");
                                      $("#bo_annual_income").css("display","block");
                                      $("#office_residence_type_id").css("display","block");
                                      $("#nature_of_business").css("display","block");

                                  }


                                }

 //checking if user is already login or not

function loginCheckStatus(){
   $(document).ready(function(){
     $.post("php/logincheck.php", function(data){
       var myobj = JSON.parse(data);

       //checking cookies whether these are set or not
       if (myobj.cookie_name != null && myobj.cookie_password != null) {
         if (myobj.cookie_name != "" && myobj.cookie_password != "") {
           $("#login-username").attr("value", myobj.cookie_name);
           $("#login-password").attr("value", myobj.cookie_password);
         }
       }

       if (myobj.status == 1) {
         //checking secure_code
         if (myobj.secure_code != 0) {
           window.location.href = "php/validate.php";
         }

         $("#username").html(myobj.username);
         $(".login").css("display", "none");
         $("#nav-1").css("display", "none");
         $("#nav-2").css("display", "block");
       }
       else if (myobj.status == 0) {
         $("#nav-2").css("display", "none");
         $(".login").css("display", "block");
         $("#nav-1").css("display", "block");
         if (myobj.modal == 1) {
           $("#myModal").modal("show");
         }
       }
     });
   });
 }


 function onClickBuyNow()
{
	var temp= this.tag;
}


$(document).ready(function(){


/* ///// PERSONAL LOAN ///// */

$("#personal_loan_form #submit").click(function(e){

	e.preventDefault();

	var elementId="#personal_loan_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 				=   $(elementId+" #service_name").val();
	var _sub_service_name 			=   $(elementId+" #sub_service_name").val();
	var _amount        				=	$(elementId+" #amount").val();
	/* Salaried Details */
	var _employment_type            =   $(elementId+" #employment_type").val();
	var _income 					=	$(elementId+" #income").val();
	var _salary_mode				=   $(elementId+"#salary_mode").val();
	var _comp_name					=	$(elementId+"#comp_name").val();
	var _prof_sub_type				=   $(elementId+"#prof_sub_type").val();
	var _prof_xp					=	$(elementId+"#prof_xp").val();
	var _annual_income				=	$(elementId+"#annual_income").val();
	var _business_sub_emp_type 		= 	$(elementId+"#business_sub_emp_type").val();
	var _annual_turnover			= 	$(elementId+"#annual_turnover").val();
	var _nature_of_business 		= 	$(elementId+"#nature_of_business").val();
	var _bo_xp						= 	$(elementId+"#bo_xp").val();
	var _bo_annual_income   		= 	$(elementId+"#bo_annual_income").val();
	var _office_residence_type_id 	= 	$(elementId+"#office_residence_type_id").val();
	/* End of Salaried Details */
	var _city_id					=   $(elementId+" #city_id").val();
	var _purpose_of_loan 			=   $(elementId+" #purpose_of_loan").val();
	var _primary_bank   			=   $(elementId+" #primary_bank").val();
	var _curr_emi					=   $(elementId+" #curr_emi").val();
	var _residence_type 			=   $(elementId+"#residence_type").val();
	var _dob	        			= 	verifyDOB(elementId);
	var _past_loan      			=   $(elementId+"#past_loan").val();
	var _name 	        			= 	verifyName(elementId);
	var _email	        			=  	verifyEmail(elementId);
	var _phone	        			= 	verifyMobileNumber(elementId);
	var _pincode        			= 	verifyPincode(elementId);

    if (_amount == -1) {
		showAlertOnMessagePost("Please Enter amount",2,elementId);
    }
    else if (_dob.trim() == "") {
		showAlertOnMessagePost("Please Enter Date Of Birth",2,elementId);
    }
	 else if (_income == -1) {
		showAlertOnMessagePost("Please Enter Income",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name				:	_service_name,
			sub_service_name 			:	_sub_service_name,
			amount 						: 	_amount,
			employment_type				: 	_employment_type,
			income 						:   _income,
			salary_mode 				:  	_salary_mode,
			comp_name 					: 	_comp_name,
			prof_sub_type 				:   _prof_sub_type,
			prof_xp   					:   _prof_xp,
			annual_income 				: 	_annual_income,
			business_sub_emp_type 		: 	_business_sub_emp_type,
			annual_turnover       		: 	_annual_turnover,
			nature_of_business    		: 	_nature_of_business,
			bo_xp    			  		: 	_bo_xp,
			bo_annual_income 	  		: 	_bo_annual_income,
			office_residence_type_id 	: 	_office_residence_type_id,
			city_id 					:   _city_id,
			purpose_of_loan 			: 	_purpose_of_loan,
			primary_bank        		:   _primary_bank,
			curr_emi           			:   _curr_emi,
			residence_type      		:   _residence_type,
			dob                 		:   _dob,
			past_loan           		:   _past_loan,
			name 						: 	_name,
			email 						: 	_email,
			phone 						: 	_phone,
			pincode 					: 	_pincode

		};


		callService(form_data,elementId);

    }
  });


/* ///// HOME LOAN TRANSFER///// */

$("#home_loan_transfer_form #submit").click(function(e){

	e.preventDefault();

	var elementId ="#home_loan_transfer_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 			=  $("#home_loan_transfer_form #service_name").val();
	var _sub_service_name 		=  $("#home_loan_transfer_form #sub_service_name").val();
	var _loan_amount_type	 	= $(elementId+"#loan_amount_type").val();
	var _loan_amount     	 	= $(elementId+"#loan_amount").val();
	var _interest_rate    		=	$(elementId+"#interest_rate").val();
	var _city_id		 		=  $(elementId+" #city_id").val();
	var _loan_year		 	 	=  $(elementId+" #loan_year").val();
	var _loan_tenure	 		=  $(elementId+" #loan_tenure").val();
	var _loan_with_current_bank =  $(elementId+" #loan_with_current_bank").val();
	var _name 	                = 	verifyName(elementId);
	var _email					=  	verifyEmail(elementId);
	var _phone					= 	verifyMobileNumber(elementId);
	var _pincode 				= 	verifyPincode(elementId);


	if (_loan_amount == -1) {
		showAlertOnMessagePost("Please Enter Outstanding Balance",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name			:	_service_name,
			sub_service_name 		:	_sub_service_name,
			loan_amount_type			: 	_loan_amount_type,
			loan_amount             :   _loan_amount,
			interest_rate           : _interest_rate ,
			city_id                 : _city_id,
			loan_year               : _loan_year,
			loan_tenure             : _loan_tenure,
			loan_with_current_bank  : _loan_with_current_bank,
			name 					: 	_name,
			//data 					: 	_stringData,
			email 					: 	_email,
			phone 					: 	_phone,
			pincode 				: 	_pincode

		};

		callService(form_data,elementId);

    }
  });

/* ///// HOME LOAN ///// */


$("#home_loan_form #submit").click(function(e){

	e.preventDefault();

	var elementId ="#home_loan_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 			      =    $("#home_loan_form #service_name").val();
	var _sub_service_name 		      =    $("#home_loan_form #sub_service_name").val();
	var _loan_type	 			      =    $(elementId+"#loan_type").val();
	var _purpose_of_loan     	 	  =    $(elementId+"#purpose_of_loan").val();
	var _builder_name   		      =	   $(elementId+"#builder_name").val();
	var _cost_home_flat		 		  =    $(elementId+" #cost_home_flat").val();
	var _sub_purpose_of_loan_id		  =    $(elementId+" #sub_purpose_of_loan_id").val();
	var _location_of_land	 		  =    $(elementId+" #location_of_land").val();
	var _cost_plot                    =    $(elementId+" #cost_plot").val();
	var _cost_construction            =    $(elementId+"#cost_construction").val();
	var _loan_amount	 	 	      =    $(elementId+" #loan_amount").val();
	var _employment_type_id	 		  =    $(elementId+" #employment_type_id").val();
	var _monthly_income        		  =    $(elementId+" #monthly_income").val();
	var _annual_income                =    $(elementId+"#annual_income").val();
	var _property_city                =    $(elementId+"#property_city ").val();
	var _city_id		 		      =    $(elementId+" #city_id").val();
	var _name 	                      =    verifyName(elementId);
	var _email	                      =    verifyEmail(elementId);
	var _phone	                      =    verifyMobileNumber(elementId);
	var _pincode                      =    verifyPincode(elementId);


	if (_loan_amount == -1) {
		showAlertOnMessagePost("Please Enter Outstanding Balance",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name				:		_service_name,
			sub_service_name 			:		_sub_service_name,
			loan_type					: 		_loan_type,
			purpose_of_loan             :   	_purpose_of_loan,
			builder_name          		: 		_builder_namesss ,
			cost_home_flat        		: 		_cost_home_flat,
			sub_purpose_of_loan_id      : 		_sub_purpose_of_loan_id,
			location_of_land      		:       _location_of_land,
			cost_plot             		:       _cost_plot,
			cost_construction    		: 		_cost_construction,
			loan_amount          		:       _loan_amount,
			employment_type_id   		: 		_employment_type_id,
			monthly_income 				: 		_monthly_income,
			annual_income         		: 		_annual_income,
			property_city         		: 		_property_city,
			city_id                	 	: 		_city_id,
			name 						: 		_name,
			//data 						: 		_stringData,
			email 						: 		_email,
			phone 						: 		_phone,
			pincode 					: 		_pincode

		};

		callService(form_data,elementId);

    }
  });

/* ///// Loan Against Property ///// */
$("#loan_against_property_form #submit").click(function(e){

	e.preventDefault();

	var elementId ="#loan_against_property_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 			      =    $("#loan_against_property_form #service_name").val();
	var _sub_service_name 		      =    $("#loan_against_property_form #sub_service_name").val();
	var _property_type	 			  =    $(elementId+"#property_type").val();
	var _cost_home_flat     	 	  =    $(elementId+"#cost_home_flat").val();
	var _property_city   		      =	   $(elementId+"#property_city").val();
	var _loan_amount	 		      =    $(elementId+" #loan_amount").val();
	var _builder_name                 =    $(elementId+"#builder_name").val();
	var _borrow_duration              =    $(elementId+"#borrow_duration").val();
	var _employment_type              =    $(elementId+" #employment_type").val();
	var _income 					  =	   $(elementId+" #income").val();
	var _salary_mode				  =    $(elementId+"#salary_mode").val();
	var _comp_name					  =	   $(elementId+"#comp_name").val();
	var _prof_sub_type				  =    $(elementId+"#prof_sub_type").val();
	var _prof_xp					  =	   $(elementId+"#prof_xp").val();
	var _annual_income				  =	   $(elementId+"#annual_income").val();
	var _business_sub_emp_type 		  =    $(elementId+"#business_sub_emp_type").val();
	var _annual_turnover			  =    $(elementId+"#annual_turnover").val();
	var _nature_of_business 		  =    $(elementId+"#nature_of_business").val();
	var _bo_xp						  =    $(elementId+"#bo_xp").val();
	var _bo_annual_income   		  =    $(elementId+"#bo_annual_income").val();
	var _office_residence_type_id 	  =    $(elementId+"#office_residence_type_id").val();
	var _city_id		 		      =    $(elementId+" #city_id").val();
	var _purpose_of_loan 			  =    $(elementId+" #purpose_of_loan").val();
	var _primary_bank			  	  =    $(elementId+" #primary_bank").val();
	var _curr_emi 			  		  =    $(elementId+" #curr_emi").val();
	var _residence_type 			  =    $(elementId+"#residence_type").val();
	var _dob	        			  = 	verifyDOB(elementId);
	var _past_loan					  =    $(elementId+"#past_loan").val();
	var _name 	                      =    verifyName(elementId);
	var _email	                      =    verifyEmail(elementId);
	var _phone	                      =    verifyMobileNumber(elementId);
	var _pincode                      =    verifyPincode(elementId);


	if (_loan_amount == -1) {
		showAlertOnMessagePost("Please Enter Outstanding Balance",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name				:	_service_name,
			sub_service_name 			:	_sub_service_name,
			property_type				: 	_property_type,
			cost_home_flat        		: 	_cost_home_flat,
			property_city				:  _property_city,
			loan_amount 				:  _loan_amount,
			builder_name 				:  _builder_name,
			borrow_duration 			:  _borrow_duration,
			employment_type				: 	_employment_type,
			income 						:   _income,
			salary_mode 				:  	_salary_mode,
			comp_name 					: 	_comp_name,
			prof_sub_type 				:   _prof_sub_type,
			prof_xp   					:   _prof_xp,
			annual_income 				: 	_annual_income,
			business_sub_emp_type 		: 	_business_sub_emp_type,
			annual_turnover       		: 	_annual_turnover,
			nature_of_business    		: 	_nature_of_business,
			bo_xp    			  		: 	_bo_xp,
			bo_annual_income 	  		: 	_bo_annual_income,
			office_residence_type_id 	: 	_office_residence_type_id,
			city_id 					: 	_city_id,
			purpose_of_loan 			: 	_purpose_of_loan,
			primary_bank 				: 	_primary_bank,
			curr_emi 					: 	_curr_emi,
			residence_type 				: 	_residence_type,
			dob 						: 	_dob,
			past_loan 					: 	_past_loan,
			name 						: 	_name,
			email 						: 	_email,
			phone 						: 	_phone,
			pincode 					: 	_pincode

		};

		callService(form_data,elementId);

    }
  });
/* ///// Buisness Loan ///// */
$("#business_loan_form #submit").click(function(e){

	e.preventDefault();

	var elementId ="#business_loan_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 			      =    $("#business_loan_form #service_name").val();
	var _sub_service_name 		      =    $("#business_loan_form #sub_service_name").val();
	var _loan_amount	 		      =    $(elementId+" #loan_amount").val();
	var _employment_type              =    $(elementId+" #employment_type").val();
	var _income 					  =	   $(elementId+" #income").val();
	var _salary_mode				  =    $(elementId+"#salary_mode").val();
	var _comp_name					  =	   $(elementId+"#comp_name").val();
	var _prof_sub_type				  =    $(elementId+"#prof_sub_type").val();
	var _prof_xp					  =	   $(elementId+"#prof_xp").val();
	var _annual_income				  =	   $(elementId+"#annual_income").val();
	var _business_sub_emp_type 		  =    $(elementId+"#business_sub_emp_type").val();
	var _annual_turnover			  =    $(elementId+"#annual_turnover").val();
	var _nature_of_business 		  =    $(elementId+"#nature_of_business").val();
	var _bo_xp						  =    $(elementId+"#bo_xp").val();
	var _bo_annual_income   		  =    $(elementId+"#bo_annual_income").val();
	var _office_residence_type_id 	  =    $(elementId+"#office_residence_type_id").val();
	var _city_id		 		      =    $(elementId+" #city_id").val();
	var _purpose_of_loan 			  =    $(elementId+" #purpose_of_loan").val();
	var _primary_bank			  	  =    $(elementId+" #primary_bank").val();
	var _curr_emi 			  		  =    $(elementId+" #curr_emi").val();
	var _residence_type 			  =    $(elementId+"#residence_type").val();
	var _dob	        			  = 	verifyDOB(elementId);
	var _past_loan					  =    $(elementId+"#past_loan").val();
	var _name 	                      =    verifyName(elementId);
	var _email	                      =    verifyEmail(elementId);
	var _phone	                      =    verifyMobileNumber(elementId);
	var _pincode                      =    verifyPincode(elementId);


	if (_loan_amount == -1) {
		showAlertOnMessagePost("Please Enter Outstanding Balance",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name				:	_service_name,
			sub_service_name 			:	_sub_service_name,
			loan_amount 				:  _loan_amount,
			employment_type				: 	_employment_type,
			income 						:   _income,
			salary_mode 				:  	_salary_mode,
			comp_name 					: 	_comp_name,
			prof_sub_type 				:   _prof_sub_type,
			prof_xp   					:   _prof_xp,
			annual_income 				: 	_annual_income,
			business_sub_emp_type 		: 	_business_sub_emp_type,
			annual_turnover       		: 	_annual_turnover,
			nature_of_business    		: 	_nature_of_business,
			bo_xp    			  		: 	_bo_xp,
			bo_annual_income 	  		: 	_bo_annual_income,
			office_residence_type_id 	: 	_office_residence_type_id,
			city_id 					: 	_city_id,
			purpose_of_loan 			: 	_purpose_of_loan,
			primary_bank 				: 	_primary_bank,
			curr_emi 					: 	_curr_emi,
			residence_type 				: 	_residence_type,
			dob 						: 	_dob,
			past_loan 					: 	_past_loan,
			name 						: 	_name,
			email 						: 	_email,
			phone 						: 	_phone,
			pincode 					: 	_pincode

		};

		callService(form_data,elementId);

    }
  });

/* ///// Car Loan ///// */
$("#car_loan_form #submit").click(function(e){

	e.preventDefault();

	var elementId ="#car_loan_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 			      		=    $("#car_loan_form #service_name").val();
	var _sub_service_name 		      		=    $("#car_loan_form #sub_service_name").val();
	var _manufacturing_year	 		      	=    $(elementId+" #manufacturing_year").val();
	var _city_id              				=    $(elementId+" #city_id").val();
	var _car_maker 					  		=	 $(elementId+" #car_maker").val();
	var _car_model				  			=    $(elementId+"#car_model").val();
	var _car_model_variants					=	 $(elementId+"#car_model_variants").val();
	var _buy_time				  			=    $(elementId+"#buy_time").val();
	var _amount					  			=	 $(elementId+"#amount").val();
	var _loan_tenure				  		=	 $(elementId+"#loan_tenure").val();
	var _past_loan 		  					=    $(elementId+"#past_loan").val();
	var _employment_type            		=    $(elementId+" #employment_type").val();
	var _income 							=	 $(elementId+" #income").val();
	var _salary_mode						=    $(elementId+"#salary_mode").val();
	var _comp_name							=	 $(elementId+"#comp_name").val();
	var _prof_sub_type						=    $(elementId+"#prof_sub_type").val();
	var _prof_xp							=	 $(elementId+"#prof_xp").val();
	var _annual_income						=	 $(elementId+"#annual_income").val();
	var _business_sub_emp_type 				= 	 $(elementId+"#business_sub_emp_type").val();
	var _annual_turnover					= 	 $(elementId+"#annual_turnover").val();
	var _nature_of_business 				= 	 $(elementId+"#nature_of_business").val();
	var _bo_xp								= 	 $(elementId+"#bo_xp").val();
	var _bo_annual_income   				= 	 $(elementId+"#bo_annual_income").val();
	var _office_residence_type_id 			= 	 $(elementId+"#office_residence_type_id").val();
	var _city_id		 		     		=    $(elementId+" #city_id").val();
	var _purpose_of_loan 			  		=    $(elementId+" #purpose_of_loan").val();
	var _primary_bank			  	  		=    $(elementId+" #primary_bank").val();
	var _curr_emi 			  		  		=    $(elementId+" #curr_emi").val();
	var _residence_type 			  		=    $(elementId+"#residence_type").val();
	var _dob	        			  		= 	 verifyDOB(elementId);
	var _terms 						  		=    $(elementId+"#terms").val();
	var _name 	                      		=    verifyName(elementId);
	var _email	                      		=    verifyEmail(elementId);
	var _phone	                      		=    verifyMobileNumber(elementId);
	var _pincode                      		=    verifyPincode(elementId);


	if (_loan_amount == -1) {
		showAlertOnMessagePost("Please Enter Outstanding Balance",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name				:	_service_name,
			sub_service_name 			:	_sub_service_name,
			manufacturing_year 				:  _manufacturing_year,
			city_id 					: 	_city_id,
			car_maker 					: 	_car_maker,
			car_model 					:   _car_model,
			car_model_variants 			:   _car_model_variants,
			buy_time 					:   _buy_time,
			amount 						:   _amount,
			loan_tenure                 :   _past_loan,
			employment_type				: 	_employment_type,
			income 						:   _income,
			salary_mode 				:  	_salary_mode,
			comp_name 					: 	_comp_name,
			prof_sub_type 				:   _prof_sub_type,
			prof_xp   					:   _prof_xp,
			annual_income 				: 	_annual_income,
			business_sub_emp_type 		: 	_business_sub_emp_type,
			annual_turnover       		: 	_annual_turnover,
			nature_of_business    		: 	_nature_of_business,
			bo_xp    			  		: 	_bo_xp,
			bo_annual_income 	  		: 	_bo_annual_income,
			office_residence_type_id 	: 	_office_residence_type_id,
			purpose_of_loan 			: 	_purpose_of_loan,
			primary_bank 				: 	_primary_bank,
			curr_emi 					: 	_curr_emi,
			residence_type 				: 	_residence_type,
			dob 						: 	_dob,
			terms 						:   _terms,
			name 						: 	_name,
			email 						: 	_email,
			phone 						: 	_phone,
			pincode 					: 	_pincode

		};

		callService(form_data,elementId);

    }
  });

/* Education Loan */

$("#education_loan_form #submit").click(function(e){

	e.preventDefault();

	var elementId ="#education_loan_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 			=  	$("#education_loan_form #service_name").val();
	var _sub_service_name 		=  	$("#education_loan_form #sub_service_name").val();
	var _country_to_study	 	= 	$(elementId+"#country_to_study").val();
	var _degree     	 		= 	$(elementId+"#degree").val();
	var _course    				=	$(elementId+"#course").val();
	var _amount		 			=  	$(elementId+" #amount").val();
	var _loan_tenure	 		=  	$(elementId+" #loan_tenure").val();
	var _past_loan 				=  	$(elementId+" #past_loan").val();
	var _city_id 				=	$(elementId+"#city_id").val();
	var _name 	                = 	verifyName(elementId);
	var _email					=  	verifyEmail(elementId);
	var _phone					= 	verifyMobileNumber(elementId);
	var _dob 					=  	verifyDOB(elementId);
	var _pincode 				= 	verifyPincode(elementId);
	var _terms   				= 	$(elementId+"#terms");


	if (_loan_amount == -1) {
		showAlertOnMessagePost("Please Enter Outstanding Balance",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name			:	_service_name,
			sub_service_name 		:	_sub_service_name,
			country_to_study		: 	_country_to_study,
			degree                  :   _degree,
			course 					:   _course,
			amount             		:   _amount,
			loan_tenure           	: 	_loan_tenure ,
			past_loan  				:   _past_loan,
			city_id                 : 	_city_id,
			name 					: 	_name,
			email 					: 	_email,
			phone 					: 	_phone,
			dob 					: 	_dob,
			pincode 				: 	_pincode,
			terms 					:   _terms

		};

		callService(form_data,elementId);

    }
  });


/* Gold Loan */

$("#gold_loan_form #submit").click(function(e){

	e.preventDefault();

	var elementId ="#gold_loan_form";
	showAlertOnMessagePost("",4,elementId);

	var _service_name 			=  	$("#gold_loan_form #service_name").val();
	var _sub_service_name 		=  	$("#gold_loan_form #sub_service_name").val();
	var _gold_amount		 			=  	$(elementId+" #gold_amount").val();
	var _amount		 			=  	$(elementId+" #amount").val();
	var _loan_tenure	 		=  	$(elementId+" #loan_tenure").val();
	var _past_loan 				=  	$(elementId+" #past_loan").val();
	var _city_id 				=	$(elementId+"#city_id").val();
	var _name 	                = 	verifyName(elementId);
	var _email					=  	verifyEmail(elementId);
	var _phone					= 	verifyMobileNumber(elementId);
	var _dob 					=  	verifyDOB(elementId);
	var _pincode 				= 	verifyPincode(elementId);
	var _terms   				= 	$(elementId+"#terms");


	if (_loan_amount == -1) {
		showAlertOnMessagePost("Please Enter Outstanding Balance",2,elementId);
	}
	else if (_name.trim() == "") {
		showAlertOnMessagePost("Please Enter Name",2,elementId);
	}
	else if (_email.trim() == "") {
		showAlertOnMessagePost("Please Enter Email",2,elementId);
	}
	else if (_phone == "") {
		showAlertOnMessagePost("Please Enter phone",2,elementId);
	}
	else if (_pincode.trim() == "") {
		showAlertOnMessagePost("Please Enter pincode",2,elementId);
    }
    else {

		var form_data =
		{
			service_name			:	_service_name,
			sub_service_name 		:	_sub_service_name,
			gold_amount 			:   _gold_amount,
			amount             		:   _amount,
			loan_tenure           	: 	_loan_tenure ,
			past_loan  				:   _past_loan,
			city_id                 : 	_city_id,
			name 					: 	_name,
			email 					: 	_email,
			phone 					: 	_phone,
			dob 					: 	_dob,
			pincode 				: 	_pincode,
			terms 					:   _terms

		};

		callService(form_data,elementId);

    }
  });

/* ///// DATA VERIFICATION ////// */


 function verifyDOB(ElementId)
 {
	var dob=$(ElementId +" #dob").val();

	return dob;
 }

 function verifyName(ElementId)
 {
	var name=$(ElementId +" #name").val();

	return name;
 }

 function verifyEmail(ElementId)
 {
	var email=$(ElementId +" #email").val();

	return email;
 }

 function verifyMobileNumber(ElementId)
 {
	var mobile=$(ElementId +" #phone").val();

	return mobile;
 }

 function verifyPincode(ElementId)
 {
	var pincode=$(ElementId +" #pincode").val();

	return pincode;
 }

 function verifyRTO(ElementId)
 {
	var rto=$(ElementId +" #rto").val();

	return rto;
 }

 function verify_reg_num(ElementId)
 {
	var elementVal=$(ElementId +" #reg_num").val();

	return elementVal;
 }

 function verifyManufecturer(ElementId)
 {
	var elementVal=$(ElementId +" #manufecturer").val();

	return elementVal;
 }

 function verify_model(ElementId)
 {
	var elementVal=$(ElementId +" #model").val();

	return elementVal;
 }

 function verify_fuel_type(ElementId)
 {
	var elementVal=$(ElementId +" #fuel_type").val();
	return elementVal;

 }

 function verify_variant(ElementId)
 {
	var elementVal=$(ElementId +" #variant").val();

	return elementVal;
 }

 function verify_reg_yr(ElementId)
 {
	var elementVal=$(ElementId +" #reg_yr").val();

	return elementVal;
 }

 function verify_insurer(ElementId)
 {
	var elementVal=$(ElementId +" #insurer").val();
	return elementVal;

 }


 /* ///  Service Call // */

 function callService(form_data,id="")
 {
	 $.post("suraj-pay2x/php/insuranceFirstForm.php", form_data, function(data){

		showAlertOnMessagePost("Data is posted",1,id);

		var jsonData = JSON.parse(data);

		if(jsonData.result == 1)
		{
			window.location.href = jsonData.url;
		}

		if(data == "Login Successful"){
          location.reload();
        }

        else if (data == "Incorrect Username or Password") {

		  $("#success").removeClass("success_none");
          if (!($("#success").hasClass("alert-display"))) {
            $("#success").addClass("alert-display");
          }
          $("#success").html(data);

        }
      });
 }


	$('#addMorePerson').on('click',function(){
		var div = $("#health_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"health_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"health_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";

		$('#health_insurance_person_details').append(singleRow);

	});

	$('#removePerson').on('click',function(){
	var div = $("#health_insurance_person_details");
	var child_count = div.children().length;
	var children = div.children();
	children[child_count-1].remove;
	});

	$('#addMoreTravelerFam').on('click',function(){
		var div = $("#travel_fam_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"travel_fam_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"travel_fam_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";
		$('#travel_fam_insurance_person_details').append(singleRow);

	});

	$('#addMoreTravelerGrp').on('click',function(){
		var div = $("#travel_grp_insurance_person_details");
		var child_count = div.children().length+1;
		var singleRow = "<div id=\"travel_grp_insurance_person_detail_row\"><label>"+child_count+" person's age</label><input type=\"text\" class=\"form-control\" placeholder=\"age\" name=\"age\" id=\"travel_grp_insurance_age"+child_count+"\" required data-validation-required-message=\"Please enter Amount.\"></div>";
		$('#travel_grp_insurance_person_details').append(singleRow);

	});


/*
	SUCCESS:1,
	INCOMPLETE_DATA:2,
	FAILED:3,
	NONE:4
*/


  function showAlertOnMessagePost(message,msg_type,id="")
  {
	if(msg_type == 1)
	{
		 $(id +" #success").removeClass("success_none");
      if (!$(id +" #success").hasClass("success")) {
        $(id +" #success").addClass("success");
      }
	}
	else if(msg_type == 2)
	{
		$(id +" #success").removeClass("success_none");
		if (!$(id +" #success").hasClass("alert-display")) {
		  $(id +" #success").addClass("alert-display");
		}
	}
	else if(msg_type == 4)
	{
		if (!$(id +" #success").hasClass("alert-display")) {
			$(id +" #success").removeClass("alert-display");
		}
		if (!$(id +" #success").hasClass("success")) {
			$(id +" #success").removeClass("success");
		}
		$(id +" #success").addClass("success_none");
	}

    $(id +" #success").html(message);
  }




});

