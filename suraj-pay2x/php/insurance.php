<?php
include '../../php/connect.php';


 if(!isset($_SESSION['insurance_offers']))
{ 
	$query = "SELECT * FROM plans";

	if ($query_run = $db->query($query)) 
	{
		if(mysqli_num_rows($query_run) > 0)
		{
			
			$associativeDataArray = array();
			$idx=0;
			
			while($row = mysqli_fetch_assoc($query_run))
			{
			
			$associativeDataArray[$idx]['plan']=$row['plan'];
			$associativeDataArray[$idx]['payout']=$row['payout'];
			$associativeDataArray[$idx]['cover']=$row['cover'];
			$associativeDataArray[$idx]['claim']=$row['claim'];			
			$associativeDataArray[$idx]['riders']=$row['riders'];
			$associativeDataArray[$idx]['premium']=$row['premium'];			
			$associativeDataArray[$idx]['plan_id']=$row['plan_id'];			
			$idx++;
				
			} 
			
			echo json_encode($associativeDataArray);
			
		}
    
	}
	
 }
else
{
	
	echo json_encode($_SESSION['insurance_offers']);
} 

?>
