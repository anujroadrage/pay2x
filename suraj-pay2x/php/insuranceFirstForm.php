<?php
include 'connect_insurance.php';
include '../../php/Utility.php';


  $db = new mysqli('localhost', 'root', 'usbw', 'insurance');
	
	
	
if (isset($_POST['service_name']) && isset($_POST['sub_service_name'])) 
{
	
	/// +++ TERM_INSURANCE *****////
	
	if($_POST['sub_service_name']=='Term_Insurance')
	{
		// Safe input is defind in PHP script
		
		$amount 	= safe_input($_POST['amount']);
		$dob 		= safe_input($_POST['dob']);
		$smoke 		= safe_input($_POST['smoke']);
		$income 	= safe_input($_POST['income']);
		$name		= safe_input($_POST['name']);
		$phone 		= safe_input($_POST['phone']);
		$email		= safe_input($_POST['email']);
		$pincode 	= safe_input($_POST['pincode']);
		
		
		if ( $amount && $dob && $income && $smoke && $name && $phone && $email && $pincode) {
			  
			$query = "INSERT INTO term_insurance (amount,smoke,income,name,phone,email,pincode,dob) VALUES('$amount', '$smoke', '$income', '$name' , '$phone' , '$email' , '$pincode', '$dob')";
			  
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " . $query_run;	
			}
			
			$_SESSION['insurance_offers'] = getTermOffers($amount,$dob,$smoke,$income);
			  
			
			$data =array(
				'result'=>'1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers']
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				
				);
			
			echo json_encode($data);
		}
	}
	
	
	/// +++ HEALTH_INSURANCE *****////
	
	if($_POST['sub_service_name']=='health_insurance')
	{
		// Safe input is defind in PHP script
		
		if(isset($_POST['member_info'])) 	$member_info 	= $_POST['member_info'];
		if(isset($_POST['income'])) 		$income 		= safe_input($_POST['income']);
		if(isset($_POST['name'])) 			$name			= safe_input($_POST['name']);
		if(isset($_POST['phone'])) 			$phone 			= safe_input($_POST['phone']);
		if(isset($_POST['email'])) 			$email			= safe_input($_POST['email']);
		if(isset($_POST['pincode'])) 		$pincode 		= safe_input($_POST['pincode']);
		

		

		if ( $member_info && $income && $name && $phone && $email && $pincode) {
			  
			$query = "INSERT INTO health_insurance (name,phone,email,pincode,income,member_info) VALUES('$name', '$phone', '$email', '$pincode' , '$income' , '$member_info' )";
			  
			if ($query_run = $db->query($query)) {
			//echo "We will contact you soon via E-mail" . $query_run;
			}

			$_SESSION['insurance_offers'] = getHealthOffers($member_info,$income);
			  
			
			$data =array(
			'result'=>'1',
			'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
			'offers' => $_SESSION['insurance_offers']
			);
			
			echo json_encode($data);
	
		}
		else {
			$data =array(
				'result'=>'0',
				
				);
			
			echo json_encode($data);
		} 
	}
	
	/// ++++ CAR_INSURANCE *****////

	if($_POST['sub_service_name']=='Car_Insurance')
	{
		// Safe input is defind in PHP script
		
		if(isset($_POST['rto'])) 			$rto 			= safe_input($_POST['rto']);
		if(isset($_POST['reg_num']))		$reg_num 		= safe_input($_POST['reg_num']);
		if(isset($_POST['manufecturer']))	$manufecturer 	= safe_input($_POST['manufecturer']);
		if(isset($_POST['model']))			$model 			= safe_input($_POST['model']);
		if(isset($_POST['fuel_type']))		$fuel_type		= safe_input($_POST['fuel_type']);
		if(isset($_POST['variant']))		$variant 		= safe_input($_POST['variant']);
		if(isset($_POST['reg_yr']))			$reg_yr			= safe_input($_POST['reg_yr']);
		if(isset($_POST['insurer']))		$insurer		= safe_input($_POST['insurer']);
		
		if(isset($_POST['name']))			$name 			= safe_input($_POST['name']);
		if(isset($_POST['phone']))			$phone			= safe_input($_POST['phone']);
		if(isset($_POST['email']))			$email			= safe_input($_POST['email']);
		if(isset($_POST['pincode']))		$pincode 		= safe_input($_POST['pincode']);
		
		
		if ( $rto && $reg_num && $manufecturer && $model && $fuel_type && $variant && $reg_yr && $insurer && $name && $phone && $email && $pincode) {
			  
			$query = "INSERT INTO car_insurance (rto,reg_num,manufecturer,model,fuel_type,variant,reg_yr,insurer,name,phone,email,pincode) VALUES('$rto', '$reg_num', '$manufecturer', '$model' , '$fuel_type', '$variant', '$reg_yr' ,'$insurer', '$name' , '$phone' , '$email' , '$pincode')";
			 
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " ;	
			}
			
			$_SESSION['insurance_offers'] = getCarOffers($rto,$reg_num,$manufecturer,$model,$fuel_type,$variant,$reg_yr,$insurer);
			  
			
			$data =array(
				'result'=>'1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers']
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				'message'=>'Insufficient Data'
				);
			
			echo json_encode($data);
		}
	}

	/// ++++ BIKE_INSURANCE ++++////

	if($_POST['sub_service_name']=='Bike_Insurance')
	{
		// Safe input is defind in PHP script
		
		if(isset($_POST['rto'])) 			$rto 			= safe_input($_POST['rto']);
		if(isset($_POST['reg_num']))		$reg_num 		= safe_input($_POST['reg_num']);
		if(isset($_POST['manufecturer']))	$manufecturer 	= safe_input($_POST['manufecturer']);
		if(isset($_POST['model']))			$model 			= safe_input($_POST['model']);
		if(isset($_POST['fuel_type']))		$fuel_type		= safe_input($_POST['fuel_type']);
		if(isset($_POST['variant']))		$variant 		= safe_input($_POST['variant']);
		if(isset($_POST['reg_yr']))			$reg_yr			= safe_input($_POST['reg_yr']);
		if(isset($_POST['insurer']))		$insurer		= safe_input($_POST['insurer']);
		
		if(isset($_POST['name']))			$name 			= safe_input($_POST['name']);
		if(isset($_POST['phone']))			$phone			= safe_input($_POST['phone']);
		if(isset($_POST['email']))			$email			= safe_input($_POST['email']);
		if(isset($_POST['pincode']))		$pincode 		= safe_input($_POST['pincode']);
		
		
		if ( $rto && $reg_num && $manufecturer && $model && $fuel_type && $variant && $reg_yr && $insurer && $name && $phone && $email && $pincode) {
			  
			$query = "INSERT INTO bike_insurance (rto,reg_num,manufecturer,model,variant,reg_yr,insurer,name,phone,email,pincode) VALUES('$rto', '$reg_num', '$manufecturer', '$model' , '$variant', '$reg_yr' ,'$insurer', '$name' , '$phone' , '$email' , '$pincode')";
			 
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " ;	
			}
			
			$_SESSION['insurance_offers'] = getCarOffers($rto,$reg_num,$manufecturer,$model,$fuel_type,$variant,$reg_yr,$insurer);
			  
			
			$data =array(
				'result'=>'1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers']
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				'message'=>'Insufficient Data'
				);
			
			echo json_encode($data);
		}
	}


	/// +++ CANCER_INSURANCE *****////
	
	if($_POST['sub_service_name']=='cancer_insurance')
	{
		// Safe input is defind in PHP script
		
		if(isset($_POST['amount'])) 	$amount 	= safe_input($_POST['amount']);
		if(isset($_POST['dob']))		$dob 		= safe_input($_POST['dob']);
		if(isset($_POST['smoke']))		$smoke 		= safe_input($_POST['smoke']);
		if(isset($_POST['income']))		$income 	= safe_input($_POST['income']);
		if(isset($_POST['name']))		$name		= safe_input($_POST['name']);
		if(isset($_POST['phone']))		$phone 		= safe_input($_POST['phone']);
		if(isset($_POST['email']))		$email		= safe_input($_POST['email']);
		if(isset($_POST['pincode']))	$pincode 	= safe_input($_POST['pincode']);
		
		
		if ( $amount && $dob && $income && $smoke && $name && $phone && $email && $pincode) {
			  
			$query = "INSERT INTO cancer_insurance (amount,smoke,income,name,phone,email,pincode,dob) VALUES('$amount', '$smoke', '$income', '$name' , '$phone' , '$email' , '$pincode', '$dob')";
			  
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " . $query_run;	
			}
			
			$_SESSION['insurance_offers'] = getTermOffers($amount,$dob,$smoke,$income);
			  
			
			$data =array(
				'result'=>'1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers']
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				
				);
			
			echo json_encode($data);
		}
	}
	
	
	/// +++ HEART_INSURANCE *****////

	if($_POST['sub_service_name']=='heart_Insurance')
	{
		// Safe input is defind in PHP script
		
		if(isset($_POST['amount'])) 	$amount 	= safe_input($_POST['amount']);
		if(isset($_POST['dob']))		$dob 		= safe_input($_POST['dob']);
		if(isset($_POST['smoke']))		$smoke 		= safe_input($_POST['smoke']);
		if(isset($_POST['income']))		$income 	= safe_input($_POST['income']);
		if(isset($_POST['name']))		$name		= safe_input($_POST['name']);
		if(isset($_POST['phone']))		$phone 		= safe_input($_POST['phone']);
		if(isset($_POST['email']))		$email		= safe_input($_POST['email']);
		if(isset($_POST['pincode']))	$pincode 	= safe_input($_POST['pincode']);
		
		
		if ( $amount && $dob && $income && $smoke && $name && $phone && $email && $pincode) {
			  
			$query = "INSERT INTO heart_insurance (amount,smoke,income,name,phone,email,pincode,dob) VALUES('$amount', '$smoke', '$income', '$name' , '$phone' , '$email' , '$pincode', '$dob')";
			 
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " ;	
			}
			
			$_SESSION['insurance_offers'] = getTermOffers($amount,$dob,$smoke,$income);
			  
			
			$data =array(
				'result'=>'1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers']
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				'message'=>'Insufficient Data'
				);
			
			echo json_encode($data);
		}
	}
	
	
		/// +++ PROPERTY_INSURANCE *****////

	if($_POST['sub_service_name']=='property_insurance')
	{
		// Safe input is defind in PHP script
		if(isset($_POST['property_cat'])) 		$property_cat 		= safe_input($_POST['property_cat']);
		if(isset($_POST['property_owner'])) 	$property_owner 	= safe_input($_POST['property_owner']);
		if(isset($_POST['age']))				$age 				= safe_input($_POST['age']);
		if(isset($_POST['property_type']))		$property_type 		= safe_input($_POST['property_type']);
		if(isset($_POST['risc_covered']))		$risc_covered 		= safe_input($_POST['risc_covered']);
		if(isset($_POST['sum'])) 				$sum 				= safe_input($_POST['sum']);
		if(isset($_POST['furniture']))			$furniture 			= safe_input($_POST['furniture']);
		if(isset($_POST['elec_break']))			$elec_break 		= safe_input($_POST['elec_break']);
		if(isset($_POST['elec_app_break']))		$elec_app_break 	= safe_input($_POST['elec_app_break']);
		if(isset($_POST['fire'])) 				$fire 				= safe_input($_POST['fire']);
		if(isset($_POST['burglary']))			$burglary 			= safe_input($_POST['burglary']);
		if(isset($_POST['secured_measures']))	$secured_measures 	= safe_input($_POST['secured_measures']);
		if(isset($_POST['name']))				$name				= safe_input($_POST['name']);
		if(isset($_POST['phone']))				$phone 				= safe_input($_POST['phone']);
		if(isset($_POST['email']))				$email				= safe_input($_POST['email']);
		if(isset($_POST['pincode']))			$pincode 			= safe_input($_POST['pincode']);
		
		
		if ($property_cat && $property_owner && $property_type && $age && $risc_covered && $sum && $furniture && $elec_break && $elec_app_break && $fire && $burglary && $secured_measures  && $name && $phone && $email && $pincode) {
			  
			$query = "INSERT INTO property_insurance (property_cat,	property_owner,age,property_type,risc_covered,sum,furniture,elec_break,elec_app_break,fire ,burglary,secured_measures,name,phone,email,pincode)" . 
										" VALUES('$property_cat',	'$property_owner','$age','$property_type','$risc_covered','$sum','$furniture','$elec_break','$elec_app_break','$fire','$burglary','$secured_measures','$name','$phone','$email','$pincode')";
			 
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " ;	
			}
			
			$_SESSION['insurance_offers'] = getPropertyOffers($property_cat ,$property_owner ,$property_type ,$age ,$risc_covered ,$sum ,$furniture ,$elec_break ,$elec_app_break ,$fire ,$burglary , $secured_measures);
			  
			
			$data =array(
				'result'=>'1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers']
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				'message'=>'Insufficient Data'
				);
			
			echo json_encode($data);
		}
	}
	
	
	/// +++ TRAVEL_FAMILY_INSURANCE *****////

	if($_POST['sub_service_name']=='travel_family_insurance')
	{
		// Safe input is defind in PHP script

		if(isset($_POST['dest_country'])) 	$dest_country 	= safe_input($_POST['dest_country']);
		if(isset($_POST['member_info']))	$member_info 	= $_POST['member_info'];
		if(isset($_POST['start_date']))		$start_date 	= safe_input($_POST['start_date']);
		if(isset($_POST['end_date']))		$end_date 		= safe_input($_POST['end_date']);
		if(isset($_POST['name']))			$name			= safe_input($_POST['name']);
		if(isset($_POST['phone']))			$phone 			= safe_input($_POST['phone']);
		if(isset($_POST['email']))			$email			= safe_input($_POST['email']);
		if(isset($_POST['pincode']))		$pincode 		= safe_input($_POST['pincode']);
		
		
		if ( $dest_country && $member_info && $start_date && $end_date && $name && $phone && $email && $pincode) {
			  $type = '0';
			$query = "INSERT INTO travel_insurance (dest_country,type,member_info,start_date,end_date,name,phone,email,pincode) VALUES('$dest_country','$type', '$member_info', '$start_date', '$end_date', '$name' , '$phone' , '$email' , '$pincode')";
			 
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " ;	
			}
			
			$_SESSION['insurance_offers'] = getTravelOffers($dest_country,$member_info,$start_date,$end_date);
			  
			
			$data =array(
				'result'=>'-1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers'],
				'message'=>$member_info
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				'message'=>'Insufficient Data'
				);
			
			echo json_encode($data);
		}
	}
	
	/// +++ TRAVEL_GROUP_INSURANCE *****////

	if($_POST['sub_service_name']=='Travel_group_Insurance')
	{
		// Safe input is defind in PHP script

		if(isset($_POST['traveler_type'])) 	$traveler_type 	= safe_input($_POST['traveler_type']);		
		if(isset($_POST['dest_country'])) 	$dest_country 	= safe_input($_POST['dest_country']);
		if(isset($_POST['member_info']))	$member_info 	= $_POST['member_info'];
		if(isset($_POST['start_date']))		$start_date 	= safe_input($_POST['start_date']);
		if(isset($_POST['end_date']))		$end_date 		= safe_input($_POST['end_date']);
		if(isset($_POST['name']))			$name			= safe_input($_POST['name']);
		if(isset($_POST['phone']))			$phone 			= safe_input($_POST['phone']);
		if(isset($_POST['email']))			$email			= safe_input($_POST['email']);
		if(isset($_POST['pincode']))		$pincode 		= safe_input($_POST['pincode']);
		
		
		if ( $dest_country && $member_info && $start_date && $end_date && $name && $phone && $email && $pincode) {
			  $type = '1';
			$query = "INSERT INTO travel_insurance (dest_country,traveler_type,type,member_info,start_date,end_date,name,phone,email,pincode) VALUES('$dest_country','$traveler_type','$type', '$member_info', '$start_date', '$end_date', '$name' , '$phone' , '$email' , '$pincode')";
			 
			if ($query_run = $db->query($query)) {
				//echo "We will contact you soon via E-mail" . $query_run;
			}
			else
			{
				echo "Query not executed  " ;	
			}
			
			$_SESSION['insurance_offers'] = getTravelOffers($dest_country,$member_info,$start_date,$end_date);
			  
			
			$data =array(
				'result'=>'-1',
				'url' => '' .BASE_PATH.'/suraj-pay2x/insurance_plan.html',
				'offers' => $_SESSION['insurance_offers']
				);
			
			echo json_encode($data);
			
			
		}
		else {
			$data =array(
				'result'=>'0',
				'message'=>'Insufficient Data'
				);
			
			echo json_encode($data);
		}
	}
	
	
	
	}
else
{

$data =array(
		'result'=>'0',
		'message'=>'service or sub-service is not defined'
		);
	
echo json_encode($data);

}

/// Return the array of Term offers on the basis of the parameter given

function getTermOffers($amount,$dob,$smoke,$income)
{
	
	$db = new mysqli('localhost', 'root', 'usbw', 'pay2x');
	$query = "SELECT * FROM plans";

	if ($query_run = $db->query($query)) 
	{
		if(mysqli_num_rows($query_run) > 0)
		{
			
			$associativeDataArray = array();
			$idx=0;
			while($row = mysqli_fetch_assoc($query_run))
			{
			
			$associativeDataArray[$idx]['plan']=$row['plan'];
			$associativeDataArray[$idx]['payout']=$row['payout'];
			$associativeDataArray[$idx]['cover']=$row['cover'];
			$associativeDataArray[$idx]['claim']=$row['claim'];			
			$associativeDataArray[$idx]['riders']=$row['riders'];
			$associativeDataArray[$idx]['premium']=$row['premium'];			
			$associativeDataArray[$idx]['plan_id']=$row['plan_id'];			
			$idx++;
				
			} 
			return $associativeDataArray;
			
		}
    
	}
	
}

function getCarOffers($rto,$reg_num,$manufecturer,$model,$fuel_type,$variant,$reg_yr,$insurer)
{
	$db = new mysqli('localhost', 'root', 'usbw', 'pay2x');
	$query = "SELECT * FROM plans";

	if ($query_run = $db->query($query)) 
	{
		if(mysqli_num_rows($query_run) > 0)
		{
			
			$associativeDataArray = array();
			$idx=0;
			while($row = mysqli_fetch_assoc($query_run))
			{
			
			$associativeDataArray[$idx]['plan']=$row['plan'];
			$associativeDataArray[$idx]['payout']=$row['payout'];
			$associativeDataArray[$idx]['cover']=$row['cover'];
			$associativeDataArray[$idx]['claim']=$row['claim'];			
			$associativeDataArray[$idx]['riders']=$row['riders'];
			$associativeDataArray[$idx]['premium']=$row['premium'];			
			$associativeDataArray[$idx]['plan_id']=$row['plan_id'];			
			$idx++;
				
			} 
			return $associativeDataArray;
			
		}
    
	}
}

function getHealthOffers($member_info,$income)
{
	
	$db = new mysqli('localhost', 'root', 'usbw', 'pay2x');
	$query = "SELECT * FROM plans";

	if ($query_run = $db->query($query)) 
	{
		if(mysqli_num_rows($query_run) > 0)
		{
			
			$associativeDataArray = array();
			$idx=0;
			while($row = mysqli_fetch_assoc($query_run))
			{
			
			$associativeDataArray[$idx]['plan']=$row['plan'];
			$associativeDataArray[$idx]['payout']=$row['payout'];
			$associativeDataArray[$idx]['cover']=$row['cover'];
			$associativeDataArray[$idx]['claim']=$row['claim'];			
			$associativeDataArray[$idx]['riders']=$row['riders'];
			$associativeDataArray[$idx]['premium']=$row['premium'];			
			$associativeDataArray[$idx]['plan_id']=$row['plan_id'];			
			$idx++;
				
			} 
			return $associativeDataArray;
			
		}
    
	}
	
}

function getPropertyOffers($property_cat ,$property_owner ,$property_type ,$age ,$risc_covered ,$sum ,$furniture ,$elec_break ,$elec_app_break ,$fire ,$burglary , $secured_measures)
{
	
	$db = new mysqli('localhost', 'root', 'usbw', 'pay2x');
	$query = "SELECT * FROM plans";

	if ($query_run = $db->query($query)) 
	{
		if(mysqli_num_rows($query_run) > 0)
		{
			
			$associativeDataArray = array();
			$idx=0;
			while($row = mysqli_fetch_assoc($query_run))
			{
			
			$associativeDataArray[$idx]['plan']=$row['plan'];
			$associativeDataArray[$idx]['payout']=$row['payout'];
			$associativeDataArray[$idx]['cover']=$row['cover'];
			$associativeDataArray[$idx]['claim']=$row['claim'];			
			$associativeDataArray[$idx]['riders']=$row['riders'];
			$associativeDataArray[$idx]['premium']=$row['premium'];			
			$associativeDataArray[$idx]['plan_id']=$row['plan_id'];			
			$idx++;
				
			} 
			return $associativeDataArray;
			
		}
    
	}
	
}

function getTravelOffers($dest_country,$member_info,$start_date,$end_date)
{
	
	$db = new mysqli('localhost', 'root', 'usbw', 'pay2x');
	$query = "SELECT * FROM plans";

	if ($query_run = $db->query($query)) 
	{
		if(mysqli_num_rows($query_run) > 0)
		{
			
			$associativeDataArray = array();
			$idx=0;
			while($row = mysqli_fetch_assoc($query_run))
			{
			
			$associativeDataArray[$idx]['plan']=$row['plan'];
			$associativeDataArray[$idx]['payout']=$row['payout'];
			$associativeDataArray[$idx]['cover']=$row['cover'];
			$associativeDataArray[$idx]['claim']=$row['claim'];			
			$associativeDataArray[$idx]['riders']=$row['riders'];
			$associativeDataArray[$idx]['premium']=$row['premium'];			
			$associativeDataArray[$idx]['plan_id']=$row['plan_id'];			
			$idx++;
				
			} 
			return $associativeDataArray;
			
		}
    
	}
	
}

?>
